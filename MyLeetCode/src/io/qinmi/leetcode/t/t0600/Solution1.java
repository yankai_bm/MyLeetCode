package io.qinmi.leetcode.t.t0600;

import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
 	对于任意num，假设共有k+1位（二进制）
 	那么对于小于2^k-1的数据可以理解为填充k个0或者1 满足没有连续的1,这部分可以递归实现 f(k)=f(k-1)+f(k-2)
 	其余考虑小于等于num大于2^k这部分数据中的有效解，由于首位已经确定了是1，次位一定不是1,对于num次位已经是1的情况，只能考虑1011……1以下的形式
 	如果num>=2^k+2^(k-1)则转化为2^(k-1)-1的解数量问题
 	如果num<2^k+2^(k-1)则转化为num-2^k的解数量问题
 	再对自身进行一次递归调用即可
 	举例1，对于数字5=b101,k=2，可以考虑 3和3以下的解 	00、01、10 共3种 
 	剩余部分由于5<6=4+2 所以转化为5-4=1的问题，共有0和1因而是2种，求和共5种
 	举例2，对于数字6=b110,k=2，可以考虑 3和3以下的解 	00、01、10 共3种 
 	剩余部分由于6>=6=4+2 所以转化为2-1=1的问题，共有0和1因而是2种，求和共5种
 	举例3，对于数字7=b111,k=2，可以考虑 3和3以下的解 	00、01、10 共3种 
 	剩余部分由于7>=6=4+2 所以转化为2-1=1的问题，共有0和1因而是2种，求和共5种
 	举例3，对于数字8=b1000,k=3，可以考虑 7和7以下的解 	由上可知5种 
 	剩余部分由于8<12=8+4 所以转化为8-8=0的问题，只有0因而是1种，求和共6种
 	举例9，对于数字8=b1001,k=3，可以考虑 7和7以下的解 	由上可知5种 
 	剩余部分由于9<12=8+4 所以转化为9-8=1的问题，共有0和1因而是2种，求和共7种
	以上递归求解，用缓存减少运算次数。
 */
public class Solution1 {
	private int[] cache;
    public int fill(int k) {
    	if(k<=1)//0返回1,1返回2
    		return k+1;
    	if(cache[k-1]<0)
    		cache[k-1]=fill(k-1);
    	if(cache[k-2]<0)
    		cache[k-2]=fill(k-2);   	
        return cache[k-1]+cache[k-2];
    }
    public int findIntegers(int num) {
    	if(num<=2)
    		return num+1;
    	int k=num,count=0,result=0;
    	while(k!=0)
    	{
    		k=k/2;
    		count++;//计算得到位数
    	}
    	k=count-1;
    	if(cache==null)
    	{
    		cache=new int[count];
    		Arrays.fill(cache, -1);
    	}
    	result=fill(k);
    	if(num>=Math.pow(2, k)+Math.pow(2, k-1))
    		result+=fill(k-1);
    	else
    		result+=findIntegers((int) (num-Math.pow(2, k)));
		return result;
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.findIntegers(11));
//		System.out.println(s.rob(new int[] {2,7,9,3,1}));
	}
}
