package io.qinmi.leetcode.x.x0665;

/**
 * @author NEO
 	解题思路： 	
	首先找到递减数，如果找到两个或以上的递减数则返回false
	如果只有一个递减数，且这个数的如果左侧大于等于右侧或者只有一侧返回true,否则返回false
	如果没有递减数则返回true;
 */
public class Solution1 {
    public boolean checkPossibility(int[] nums) {
        int dec=-1,len=nums.length;
        for(int i=0;i<len-1;i++)
        	if(nums[i]>nums[i+1])
        		if(dec<0)
        		{
        			dec=i;
        		}
        		else
        			return false;

        if(dec>0)
		{
			if (dec == 0 || dec == len - 2)
				return true;
			if (nums[dec - 1] <= nums[dec + 1])
				return true;
				
			if (nums[dec] <= nums[dec + 2])
				return true;
			else
				return false;
		}
        return true;
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.checkPossibility(new int[]{-1,4,2,3}));
//		System.out.println(s.checkPossibility(new int[]{2,3,3,2,4}));
	}
}
