package io.qinmi.leetcode.n.n0415;

/**
 * @author NEO
 	解题思路：
	模拟竖式计算加法过程，逐个位数相加，注意进位，另外注意右对齐
	在写字符串乘法的时候好像就顺手实现了一个
 */
public class Solution1 {
	public String addStrings(String a, String b) {
		StringBuilder sb = new StringBuilder();
		int alen = a.length();
		int blen = b.length();
		int len = alen > blen ? alen : blen;
		int carry = 0;
		for (int i = 0; i < len; i++) {
			int va = (alen - 1 - i >= 0) ? (a.charAt(alen - 1 - i) - '0') : 0;
			int vb = (blen - 1 - i >= 0) ? (b.charAt(blen - 1 - i) - '0') : 0;
			sb.append((va + vb + carry) % 10);
			carry = (va + vb + carry) / 10;
		}
		if (carry > 0)
			sb.append(carry);
		return sb.reverse().toString();
	}

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.addStrings("2", "3"));
	}
}
