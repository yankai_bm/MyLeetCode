package io.qinmi.leetcode.n.n0420;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
 	重写一下规则：
	1.由至少6个，.至多20个字符组成。
	2.至少包含一个小写字母，一个大写字母，和一个数字。
	3.同一字符不能连续出现三次 (比如 "...aaa..." 是不允许的, 但是 "...aa...a..." 是可以的)。
	针对规则1，如果少于6个law1[1]，需要增加字符 6-l，针对规则1第二款law1[0]，如果多于20个，需要删除字符 删除字符数量是 l-20 
	针对规则2，可以增加字符来实现，也可以替换字符来实现 缺少类型 k，则需要至少增加+删除=k
	针对规则3，可以增加或者替换字符（隔开），还可以用删除的方式来改善 替换或者插入的数量 t/3， 删除次数则需要t-3,如果三种方法都使用的话，应当先删除，再替换或插入
	以上各个规则可以同时修正，相当于求解一个次数最少的集合
	1规则相当于是确定的方案，而变化的主要在3、2，可以用递归的方案来考虑
 */
public class Solution1 {
	private int min=Integer.MAX_VALUE;
    public int strongPasswordChecker(String s) {
    	int[] law1=checkLen(s);//根据长度判断规则1
        int[] law2=checkChars(s);//根据字符组成判断规则2
        int[] law3=checkLaw3(s);//判断字符串连续情况        
        change(law1,law2,law3,0);
        return min;
    }
    private int[] checkLen(String s)
    {
    	int[] law1=new int[2];
    	int len=s.length();
        if(len>20)
        	law1[0]=len-20;
        if(len<6)
        	law1[1]=6-len;
        return law1;
    }
    private int[] checkLaw3(String s)
    {
    	ArrayList<Integer> result=new ArrayList<Integer>();
    	char current_char=0;
    	int count=0;
    	char[] chars=s.toCharArray();
        for(int i=0;i<chars.length;i++)
        {
        	if(current_char==chars[i])
        		count++;
        	if(i==chars.length-1||current_char!=chars[i])
        	{
        		if(count>=3)
        			result.add(count);
        		count=1;
        	}
        	current_char=chars[i];
        }
        int[] law3=new int[result.size()];
        for(int i=0;i<law3.length;i++)
        	law3[i]=result.get(i);
        return law3;
    }
    private boolean empty(int[] law)
    {
        for(int i=0;i<law.length;i++)
        	if(law[i]>0)
        		return false;
    	return true;
    }
    private void change(int[] law1,int[] law2,int[] law3,int count)
	{
		if (count >= min)
			return;
		if (empty(law1) && law2[0] == 0 && empty(law3)) {// 匹配成功
			if (min > count)
				min = count;
			return;
		}
		int[] law1_bak = Arrays.copyOf(law1, law1.length);
		int[] law2_bak = Arrays.copyOf(law2, law2.length);
		int[] law3_bak = Arrays.copyOf(law3, law3.length);
		// 第三条规则比较容易找到较低的解，从而降低搜素量
		if (!empty(law3)) {
			update(law1, law2, law3);
			change(law1, law2, law3, count + 1);
			rebase(law1, law3, law1_bak, law3_bak, law2, law2_bak);
			insert(law1, law2, law3);
			change(law1, law2, law3, count + 1);
			law2 = law2_bak;
			rebase(law1, law3, law1_bak, law3_bak, law2, law2_bak);
			delete(law1, law2, law3);
			change(law1, law2, law3, count + 1);
			law2 = law2_bak;
			rebase(law1, law3, law1_bak, law3_bak, law2, law2_bak);
		} else if (law2[0] > 0) {
			update(law1, law2, law3);
			change(law1, law2, law3, count + 1);
			law2 = law2_bak;
			rebase(law1, law3, law1_bak, law3_bak, law2, law2_bak);
			insert(law1, law2, law3);
			change(law1, law2, law3, count + 1);
			law2 = law2_bak;
			rebase(law1, law3, law1_bak, law3_bak, law2, law2_bak);
		} else if (!empty(law1)) {
			if (law1[0] > 0) {
				delete(law1, law2, law3);
				change(law1, law2, law3, count + 1);
				law2 = law2_bak;
				rebase(law1, law3, law1_bak, law3_bak, law2, law2_bak);
			}
			if (law1[1] > 0) {
				insert(law1, law2, law3);
				change(law1, law2, law3, count + 1);
				law2 = law2_bak;
				rebase(law1, law3, law1_bak, law3_bak, law2, law2_bak);
			}
		}
	}
    private void rebase(int[] law1,int[] law3,int[] law1_bak,int[] law3_bak, int[] law2, int[] law2_bak)
    {
		System.arraycopy(law1_bak, 0, law1, 0, law1_bak.length);
		System.arraycopy(law2_bak, 0, law2, 0, law2_bak.length);
		System.arraycopy(law3_bak, 0, law3, 0, law3_bak.length);
    }
    private void delete(int[] law1,int[] law2,int[] law3)
    {
    	if(law1[0]>0)
    		law1[0]--;
    	if(law1[1]>0)
    		law1[1]++;
    	for(int i=0;i<law3.length;i++)
    		if(law3[i]>0)
    		{
    			if(--law3[i]<3)
    				law3[i]=0;
    			break;
    		}
    }
    private void insert(int[] law1,int[] law2,int[] law3)
    {
    	if(law1[0]>0)
    		law1[0]++;
    	if(law1[1]>0)
    		law1[1]--;
    	if(law2[0]>0)
    		law2[0]--;
    	for(int i=0;i<law3.length;i++)
    		if(law3[i]>0)
    		{
    			law3[i]=law3[i]-3;
    			if(law3[i]<3)
    				law3[i]=0;
    			break;
    		}
    }
    private void update(int[] law1,int law2[],int[] law3)
    {//如果
    	if(law2[0]>0)
    		law2[0]--;
    	for(int i=0;i<law3.length;i++)
    		if(law3[i]>0)
    		{
    			law3[i]=law3[i]-3;
    			if(law3[i]<3)
    				law3[i]=0;
    			break;
    		}
    }

    private int[] checkChars(String s)
    {
        int lower_case=1;
        int upper_case=1; 
        int numbers=1; 
        for(char c:s.toCharArray())
        {
        	if(c>='0'&&c<='9')numbers=0;
        	if(c>='A'&&c<='Z')upper_case=0;
        	if(c>='a'&&c<='z')lower_case=0;
        }
        return new int[] { lower_case+upper_case+numbers};
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.strongPasswordChecker("ABABABABABABABABABAB1"));
	}
}
