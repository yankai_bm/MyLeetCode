package io.qinmi.leetcode.z.z0929;

import java.util.HashSet;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
	先分割，然后重新组合，再set排重
 */
public class Solution1 {
    public int numUniqueEmails(String[] emails) {
    	Set<String> set=new HashSet<String>();
        for(String s:emails)
        {
        	int index=s.indexOf("@");
        	String domain=s.substring(index);
        	String local=s.substring(0,index);
        	int plus=local.indexOf("+");
        	if(plus>=0)
        		local=s.substring(0,plus);
        	local=local.replaceAll(".", "");
        	set.add(local+domain);
        }
        return set.size();
    }
    
	public static void main(String[] args) {
		Solution1 s1=new Solution1();

		System.out.println(s1.numUniqueEmails(new String[] {"yankai.thu@gmail.com","yankaithu@gmail.com"}));
	}

}
