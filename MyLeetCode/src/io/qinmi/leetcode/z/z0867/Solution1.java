package io.qinmi.leetcode.z.z0867;

/**
 * @author NEO
 	解题思路：
	就是赋值操作而已啊
 */
public class Solution1 {
    public int[][] transpose(int[][] A) {
    	int[][] B=new int[A[0].length][A.length];
    	for(int i=0;i<A.length;i++)
    		for(int j=0;j<A[i].length;j++)
    			B[j][i]=A[i][j];    			
    	return B;
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		int[][] A=new int[][] {{1,2,3},{4,5,6}};
		int[][] B=s.transpose(A);
		
	}

}
