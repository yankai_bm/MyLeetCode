package io.qinmi.leetcode.z.z0840;

/**
 * @author NEO
 	解题思路：
	遍历幻方的左上角,由于必须是1-9组成，所以中间位置一定是5
 */
public class Solution1 {
		private int[][] grid;
	    public int numMagicSquaresInside(int[][] grid) {
	    	this.grid=grid;
	    	int len=grid.length,count=0;
	        for(int i=0;i<len-2;i++)
		        for(int j=0;j<len-2;j++)
		        {
		        	if(grid[i+1][j+1]!=5)
		        		continue;
		        	if(!checkDiff(i,j))
		        		continue;
		        	//两条对角线
		        	int sum=grid[i][j]+grid[i+1][j+1]+grid[i+2][j+2];
		        	if(sum!=grid[i][j+2]+grid[i+1][j+1]+grid[i+2][j]) continue; 
		        	//三条横线
		        	if(sum!=grid[i][j]+grid[i][j+1]+grid[i][j+2]) continue; 
		        	if(sum!=grid[i+1][j]+grid[i+1][j+1]+grid[i+1][j+2]) continue; 
		        	if(sum!=grid[i+2][j]+grid[i+2][j+1]+grid[i+2][j+2]) continue; 
		        	//三条竖线
		        	if(sum!=grid[i][j]+grid[i+1][j]+grid[i+2][j]) continue; 
		        	if(sum!=grid[i][j+1]+grid[i+1][j+1]+grid[i+2][j+1]) continue; 
		        	if(sum!=grid[i][j+2]+grid[i+1][j+2]+grid[i+2][j+2]) continue; 
		        	count++;
		        }
	        return count;
	    }
	    public boolean checkDiff(int x,int y)
	    {
	    	int[] box=new int[9];
	        for(int i=x;i<x+3;i++)
		        for(int j=y;j<y+3;j++)
		        {
			        if(grid[i][j]>9||grid[i][j]==0)
			        	return false;
		        	if(++box[grid[i][j]-1]>1)
		        		return false;	        	
		        }
	        return true;
	    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.numMagicSquaresInside(new int[][] {{4,3,8,4},{9,5,1,9},{2,7,6,2}}));
	}

}
