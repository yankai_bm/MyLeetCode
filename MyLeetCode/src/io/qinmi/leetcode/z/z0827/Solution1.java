package io.qinmi.leetcode.z.z0827;

import java.util.HashSet;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;

/**
 * @author NEO
 	解题思路：
	先求出当前地图上各块的岛面积，然后逐个遍历各个海洋点变成陆地之后的样子
 */
public class Solution1 {
	private class Point {
		int x;
		int y;
		Point(int a, int b) {
			x = a;
			y = b;
		}
	    public boolean equals(Object anObject) {
	        if (this == anObject) 
	            return true;
	        if (null == anObject) 
	            return false;
	        if (anObject instanceof Point) {
	        	Point p=(Point)anObject;
	        	return x==p.x&&y==p.y;
	        }
	        return false;
	    }
		public int hashCode() {
	    	return (x+"_"+y).hashCode();
	    }
	}
    public int largestIsland(int[][] grid) {
    	int max=0,g=grid.length;
        Set<Point>[][] set=new Set[g][];
        for(int i=0;i<g;i++)
        {
        	set[i]=new Set[g];
            for(int j=0;j<g;j++)
            {
            	set[i][j]=new HashSet<Point>();
            	set[i][j].add(new Point(i,j));
            }
        }
        for(int i=0;i<g;i++)
        {
            for(int j=0;j<g;j++)
			{//向右和向下扩张岛
				if (i < g - 1 && grid[i][j] == 1 && grid[i + 1][j] == 1) {
            		Set<Point> all=set[i][j];
            		all.addAll(set[i+1][j]);
            		for(Point k:all)
            			set[k.x][k.y]=all;
				}
				if (j < g - 1 && grid[i][j] == 1 && grid[i][j + 1] == 1) {
            		Set<Point> all=set[i][j];
            		all.addAll(set[i][j+1]);
            		for(Point k:all)
            			set[k.x][k.y]=all;
				}
			}
        }
    	for(int i=0;i<g;i++)
            for(int j=0;j<g;j++)
        		max=Math.max(max,set[i][j].size());    	
    	for(int i=0;i<g;i++)
            for(int j=0;j<g;j++)
            {
            	if(grid[i][j]==0)
            	{//逐个方向判断即可
            		Set s=new HashSet<Point>();
            		s.add(new Point(i,j));
            		if(i>0&&grid[i-1][j]==1)//上
            			s.addAll(set[i-1][j]);
            		if(i<g-1&&grid[i+1][j]==1)//下
            			s.addAll(set[i+1][j]);
            		if(j>0&&grid[i][j-1]==1)//左
            			s.addAll(set[i][j-1]);
            		if(j<g-1&&grid[i][j+1]==1)//右
            			s.addAll(set[i][j+1]);
            		max=Math.max(max,s.size());
            	}
            }
    	return max;
    }
    
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.largestIsland(new int[][] {{1, 1},{1, 1}}));
	}

}
