package io.qinmi.leetcode.z.z0866;

import java.util.HashSet;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
 	先找素数，再判断是否回文数，很可能会超时
 	排除偶数，排除3的倍数，排除偶数位的回文数（被11整除）
 */
public class Solution1 {
	//素数列表
	private Set<Integer> primes=new HashSet<Integer>();
	//素数范围,判断一个数是不是素数只需要判断所有比这个数的平方根小的素数能否整除即可
	private int prime_range=2;

    public void initPrimes() {
    	primes=new HashSet<Integer>();
    	primes.add(2);
    	prime_range=2;
    }
	public boolean isPalindrome(int x) {
		int temp = x, l = 0;
		int[] n = new int[10];// 最大的int就是10位
		while (temp > 0) {
			n[l] = temp % 10;
			temp = temp / 10;
			l++;
		}
		for (int i = 0; i < l / 2; i++)
			if (n[i] != n[l - 1 - i])
				return false;
		return true;
	}

	public int primePalindrome(int N) {
		if (N <= 11) { // 单独处理 11 以下的数，包括 11
			if (N <= 2)
				return 2;
			else if (N == 3)
				return 3;
			else if (N <= 5)
				return 5;
			else if (N <= 7)
				return 7;
			else
				return 11;
		}

		initPrimes();
		getPrimes(N);
		if (N % 2 == 0)
			N++;
		while (!(isPrimes(N)&&isPalindrome(N)  ))
		{
			N = N + 2;
			if(N%3==0)N=N+2;
			//偶数位回文数不考虑是质数的情况
			if (N <= 100 && N >= 10)
				return primePalindrome(101);
			if (N <= 10000 && N >= 1000)
				return primePalindrome(10001);
			if (N <= 1000000 && N >= 100000)
				return primePalindrome(1000001);
			if (N <= 100000000 && N >= 10000000)
				return primePalindrome(100000001);
			getPrimes(N);
		}
		return N;
	}
    public void getPrimes(int N) {
		double q=Math.ceil(Math.sqrt(N));
    	for(int i=prime_range+1;i<=q;i=i+2)
    	{
    		boolean p=true;
    		for (Integer j : primes){
				if (i % j == 0 && j != i) {
					p = false;
					break;
				}
			}
    		if(p)
    			primes.add(i);
    	}
    	prime_range=N;
    }
	public boolean isPrimes(int N) {
		int q=(int) Math.ceil(Math.sqrt(N));
		for (Integer i : primes)
		{
			if(i>q)
				continue;
			if (N % i == 0&&N!=i)
				return false;
		}
		return true;
	}

	public static void main(String[] args) {
		Solution1 s=new Solution1();
//		System.out.println(s.primePalindrome(3));
//		System.out.println(s.primePalindrome(2));
//		System.out.println(s.primePalindrome(1));
//		System.out.println(s.primePalindrome(6));
//		System.out.println(s.primePalindrome(8));
//		System.out.println(s.primePalindrome(13));
//		System.out.println(s.primePalindrome(930));		
//		System.out.println(s.primePalindrome(1215122));
//		System.out.println(s.primePalindrome(9938400));
//		System.out.println(s.primePalindrome(9989900));
		s.getPrimes(9989900);
		for(Integer x: s.primes)
			System.out.println(x);
	}

}
