package io.qinmi.leetcode.z.z0866;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * @author NEO
 	解题思路：
 	回文数分布很有特征，所以不需要按照2或者6的步长来计算，可以用
 	偶数位的回文数都能被11整除，因而除了11，偶数位的回文数都不是质数 
 */
public class Solution2 {
	//素数列表
	private Set<Integer> primes=new HashSet<Integer>();
	//素数范围,判断一个数是不是素数只需要判断所有比这个数的平方根小的素数能否整除即可
	private int prime_range=2;

    public void initPrimes() {
    	primes=new HashSet<Integer>();
    	primes.add(2);
    	prime_range=2;
    }
    public int reverse(int num) {
    	int p=num>0?1:-1;
    	num=Math.abs(num);
    	Stack<Integer> s=new Stack<Integer>();
    	while(num>0)
    	{
    		s.push(num%10);
    		num=num/10;
    	}
    	long res=0,j=0;
    	while(!s.isEmpty())
		{
			// 少反转一位
			int temp = s.pop();
			if (!s.isEmpty())
				res = (long) (temp * Math.pow(10, j++) + res);
		}
    	res=res*p;
    	return res>Integer.MAX_VALUE||res<Integer.MIN_VALUE?0:(int)res;
    }
	public int[] split(int N)
	{
		int temp = N, l = 0;
		int[] n = new int[11];// 最大的int就是10位,数组第一个存位数
		while (temp > 0) {
			l++;
			n[l] = temp % 10;
			temp = temp / 10;
		}
		n[0]=l;
		return n;
	}
	public int nextPalindrome(int x) {
		if (x <= 100 && x >= 10)
			return 101;
		if (x <= 10000 && x >= 1000)
			return 10001;
		if (x <= 1000000 && x >= 100000)
			return 1000001;
		if (x <= 100000000 && x >= 10000000)
			return 100000001;
		int[] c =split(x);// 最大的int就是10位
		int l=c[0];
		int n=getLeft(c);
		int r=reverse(n);
		int res= (int) (n*Math.pow(10, l/2)+r);
		if(res<x)
		{
			res=(int) ((n+1)*Math.pow(10, l/2)+reverse(n+1));
		}
		return res;
	}
	int getLeft(int[] n)
	{
		int l=n[0],res=0;
		for (int i = l; i > l / 2; i--)
			res=res*10+n[i];
		return res;
	}

	public int primePalindrome(int N) {
		if (N <= 11) { // 单独处理 11 以下的数，包括 11
			if (N <= 2)
				return 2;
			else if (N == 3)
				return 3;
			else if (N <= 5)
				return 5;
			else if (N <= 7)
				return 7;
			else
				return 11;
		}
		int n=nextPalindrome(N);
		initPrimes();
		getPrimes(n);
		while(!isPrimes(n))
		{
			n=nextPalindrome(n+1);	
			getPrimes(n);
		}
		return n;
	 
	}
    public void getPrimes(int N) {
		int q=(int) Math.ceil(Math.sqrt(N));
    	for(int i=prime_range+1;i<=q;i=i+2)
    	{
    		boolean p=true;
    		for (Integer j : primes){
				if (i % j == 0 && j != i) {
					p = false;
					break;
				}
			}
    		if(p)
    			primes.add(i);
    	}
    	prime_range=q;
    }
	public boolean isPrimes(int N) {
		int q=(int) Math.ceil(Math.sqrt(N));
		for (Integer i : primes)
		{
			if(i>q)
				continue;
			if (N % i == 0&&N!=i)
				return false;
		}
		return true;
	}

	public static void main(String[] args) {
		Solution2 s=new Solution2();		
		System.out.println(s.primePalindrome(930));
	}

}
