package io.qinmi.leetcode.z.z0906;

/**
 * @author NEO
 	解题思路：
	回文数的平方仍然是回文数，则平方后的数就是超级回文数，这样可以少生成很多回文数
 */
public class Solution2 {
	private int odd;
	private String base;
    public String reverse(String base,int odd) 
    {//反转n产生一个回文数，如果是奇数则为以最后一位为对称轴反转
		StringBuilder sb=new StringBuilder();
		sb.append(base);
		for(int i=base.length()-1-odd;i>=0;i--)
			sb.append(base.charAt(i));
    	return sb.toString();
    }
    public String inc(String base) 
	{// 
		int carry = 1;
		char[] chars = base.toCharArray();
		for (int i = chars.length - 1; i >= 0; i--) {
			chars[i] = (char) (chars[i] + carry);
			if (chars[i] > '9') {
				chars[i] = '0';
				carry = 1;
			} else
				carry = 0;
			if (carry == 0)
				break;
		}
		if (carry == 1)
			return "1" + new String(chars);
		else
			return new String(chars);
	}
    public long compare(String n1,String n2) 
    {
    	return Long.parseLong(n1)-Long.parseLong(n2);
    }
    public String nextPalindromic() 
    {
    	String new_base=inc(base);
    	if(new_base.length()==base.length())
    	{
    		base=new_base;
    		return reverse(base,odd);
    	}
    	else
    	{
    		if(odd==0)
    			base=new_base;
    		else
    			base=new_base.substring(0, base.length());
    		odd=(odd+1)%2;    		
    		return reverse(base,odd);    		
    	}
    }
    public int superpalindromesInRange(String L, String R) {
    	long l=Long.parseLong(L),r=Long.parseLong(R);
    	if(l>r)    		return 0;
    	String sqrtL=String.valueOf((long)Math.sqrt(l));
    	int count=0,len=sqrtL.length();
    	int half_len=(len+1)/2;
    	odd=len%2;
    	base=sqrtL.substring(0, half_len);
    	long number=Long.parseLong(reverse(base,odd));
    	if(number*number<l)
    		number=Long.parseLong(nextPalindromic());
    	while(number*number<=r)
    	{
    		if(isPalindrome(number*number))
    			++count;
    		number=Long.parseLong(nextPalindromic());
    	}
        return count;
    }
    public boolean isPalindrome(long n)
    {
    	String num=String.valueOf(n);
    	for(int i=0;i<num.length()/2;i++)    	
    		if(num.charAt(i)!=num.charAt(num.length()-1-i))
    			return false;
    	return true;
    }
	public static void main(String[] args) {
		Solution2 s=new Solution2();
		System.out.println(s.superpalindromesInRange("462","154370712829064"));
	}

}
