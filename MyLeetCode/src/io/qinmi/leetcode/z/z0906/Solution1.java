package io.qinmi.leetcode.z.z0906;

/**
 * @author NEO
 	解题思路：
	回文数的产生可以用这样的过程来考虑，
	对于位数为k的自然数作为基数，先以最后一位为对称轴反转其余位，拼接到原有自然数的后方，形成奇数位的自然数，直到全部是9的情形
	再将基数返回至k位一个1和k-1个0的形式，直接反转所有位数，拼接到原有自然数的后方形成偶数位的自然数，直到全部是9的情形
	再将基数升至k+1位，重复上述过程
	因而一般思路可以考虑找出L到R之间的所有回文数，并逐个判断是否超级回文数，然而，这样会超时	
 */
public class Solution1 {
	private int odd;
	private String base;
    public String reverse(String base,int odd) 
    {//反转n产生一个回文数，如果是奇数则为以最后一位为对称轴反转
		StringBuilder sb=new StringBuilder();
		sb.append(base);
		for(int i=base.length()-1-odd;i>=0;i--)
			sb.append(base.charAt(i));
    	return sb.toString();
    }
    public String inc(String base) 
	{// 
		int carry = 1;
		char[] chars = base.toCharArray();
		for (int i = chars.length - 1; i >= 0; i--) {
			chars[i] = (char) (chars[i] + carry);
			if (chars[i] > '9') {
				chars[i] = '0';
				carry = 1;
			} else
				carry = 0;
			if (carry == 0)
				break;
		}
		if (carry == 1)
			return "1" + new String(chars);
		else
			return new String(chars);
	}
    public long compare(String n1,String n2) 
    {
    	return Long.parseLong(n1)-Long.parseLong(n2);
    }
    public String nextPalindromic() 
    {
    	String new_base=inc(base);
    	if(new_base.length()==base.length())
    	{
    		base=new_base;
    		return reverse(base,odd);
    	}
    	else
    	{
    		if(odd==0)
    			base=new_base;
    		else
    			base=new_base.substring(0, base.length());
    		odd=(odd+1)%2;    		
    		return reverse(base,odd);    		
    	}
    }
    public int superpalindromesInRange(String L, String R) {
    	if(Long.parseLong(L)>Long.parseLong(R))
    		return 0;
    	int count=0,l=L.length();
    	int half_len=(l+1)/2;
    	odd=l%2;
    	base=L.substring(0, half_len);
    	String number=reverse(base,odd);
    	if(compare(number,L)<0)
    		number=nextPalindromic();
    	while(compare(number,R)<=0)
    	{
    		if(isSuperPalindrome(number))
    			System.out.println(++count+":"+number);
    		
    		number=nextPalindromic();
    	}
        return count;
    }
    public boolean isSuperPalindrome(String s)
    {
    	long n=Long.parseLong(s);
    	long q=(long) Math.sqrt(n);
    	if(n!=q*q)
    		return false;
    	return isPalindrome(String.valueOf(q));
    }
    public boolean isPalindrome(String num)
    {
    	for(int i=0;i<num.length()/2;i++)    	
    		if(num.charAt(i)!=num.charAt(num.length()-1-i))
    			return false;
    	return true;
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.superpalindromesInRange("462","154370712829064"));
	}

}
