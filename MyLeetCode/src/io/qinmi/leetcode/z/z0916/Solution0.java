package io.qinmi.leetcode.z.z0916;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author KaiYan
 *解题思路：两个字符串序列都排序，然后比较，会超时
 */
public class Solution0 {

    public boolean isSub(String a,String b) {
		int j=0;
		for(int i=0;i<a.length()&&j<b.length();)
		{
			if(a.charAt(i)==b.charAt(j))
			{
				i++;
				j++;
			}
			else if(a.charAt(i)>b.charAt(j))
			{
				return false;
			}
			else
			{
				i++;				
			}
		}
		if(j==b.length())
			return true;
		return false;
    }
    public String[] init(String[] A) {
    	String[] B=new String[A.length];
    	for(int i=0;i<A.length;i++)
    	{
    		char[] c=A[i].toCharArray();
    		Arrays.sort(c);
    		B[i]=new String(c);
    	}
    	return B;
    }
    public List<String> wordSubsets(String[] A, String[] B) {
    	String[] C=A;
    	A=init(A);
    	B=init(B);
    	List<String> result=new ArrayList<String>();
    	for(int i=0;i<A.length;i++)
    	{
    		boolean isSub=true;
        	for(int j=0;j<B.length;j++)
        	{
            	if(!isSub(A[i], B[j]))
            	{
            		isSub=false;
            		break;
            	}        		
        	}
        	if(isSub)
        		result.add(C[i]);
    	}
    	
		return result;        
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Solution0 s=new Solution0();
		System.out.println(s.init(new String[]{"facebook","ceo"})[0]);
		System.out.println(s.isSub("abcefkoo","ceo"));
	}

}
