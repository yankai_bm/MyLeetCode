package io.qinmi.leetcode.z.z0839;

import java.util.HashSet;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
	每个点对应一个Set，两点相似则set合并，不断合并，直至遍历所有的组合，最终剩余的set数就是答案
 */
public class Solution1 {
    public int numSimilarGroups(String[] A) {
        Set<Integer>[] set=new Set[A.length];
        for(int i=0;i<A.length;i++)
        {
        	set[i]=new HashSet<Integer>();
        	set[i].add(i);
        }
        for(int i=0;i<A.length-1;i++)
        {
            for(int j=i+1;j<A.length;j++)
            {
            	if((!set[i].contains(j))&&similar(A[i],A[j]))
            	{
            		Set<Integer> all=set[i];
            		all.addAll(set[j]);
            		for(Integer k:all)
            		{
            			set[k]=all;
            		}
            	}
            	
            }        	
        }
        Set<Set<Integer>> total=new HashSet<Set<Integer>>();
		for(Set<Integer> one:set)
		{
			total.add(one);
		}
        return total.size();
    }
    private boolean similar(String s1,String s2)
    {
    	int count=0;
    	int[] index=new int[2];
    	for(int i=0;i<s1.length();i++)
    	{
    		if(s1.charAt(i)!=s2.charAt(i))
    		{
    			if(count>=2)
    				return false;
    			index[count++]=i;
    		}
    	}
    	return s1.charAt(index[0])==s2.charAt(index[1])&&s1.charAt(index[1])==s2.charAt(index[0]);  	
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.numSimilarGroups(new String[] {"omv","ovm"}));
	}

}
