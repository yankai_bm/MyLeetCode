package io.qinmi.leetcode.z.z0771;

/**
 * @author NEO
 	解题思路：
	逐个挑选就行了
 */
public class Solution1 {
    public int numJewelsInStones(String J, String S) {
        char[] jChars=J.toCharArray();
        char[] sChars=S.toCharArray();
        int count=0;
        for(char c1:jChars)
            for(char c2:sChars)
            	if(c1==c2)count++;
        return count;
    }
    
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.numJewelsInStones("Aa","aAAbbbb"));
	}

}
