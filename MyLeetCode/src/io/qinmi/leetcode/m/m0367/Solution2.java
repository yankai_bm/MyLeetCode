package io.qinmi.leetcode.m.m0367;

/**
 * @author NEO
 	解题思路：
	牛顿迭代法
 */
public class Solution2 {
    public boolean isPerfectSquare(int x) {
    	if(x<=1)return true;
    	long result=x;
    	while(x/result<result)
    	    result= (result + x / result)/2;
    	return result*result==x;    		
    }

    public static void main(String[] args) {
    	Solution2 s = new Solution2();		
		System.out.println(s.isPerfectSquare(9));
	}
}
