package io.qinmi.leetcode.m.m0367;

/**
 * @author NEO
 	解题思路：
	简单朴素的想法，找到0-46340中的r使得r*r==x即可，46340再大会超过int值域，引入一个temp变量减少乘法运算
 */
public class Solution1 {
    public boolean isPerfectSquare(int x) {
    	int temp=0;
    	for(int i=0;i<=46340&&temp<x;i++)
    		if((temp=i*i)==x)
    			return true;
    	return false;    		
    }

    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.isPerfectSquare(10));
	}
}
