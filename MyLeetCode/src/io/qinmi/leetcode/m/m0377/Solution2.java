package io.qinmi.leetcode.m.m0377;
import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
	combinationSum4({a1,a2,a3……an},target)=
	combinationSum4({a1,a2,a3……an},target-a1)+
	combinationSum4({a1,a2,a3……an},target-a2)+
	combinationSum4({a1,a2,a3……an},target-a3)+
	……
	combinationSum4({a1,a2,a3……an},target-an)
	
	保存计算结果以备使用
 */
public class Solution2 {
	private int[] result;

    public int combinationSum4(int[] nums, int target) {
        result = new int[target + 1];
        Arrays.fill(result, -1);
        result[0] = 1;
        return search(nums, target);
    }

    private int search(int[] nums, int target) {
        if (result[target] != -1) {
            return result[target];
        }
        int res = 0;
        for (int num : nums) {
            if (target >= num) {
                res += search(nums, target - num);
            }
        }
        result[target] = res;
        return res;
    }
    
	public static void main(String[] args) {
		Solution2 s = new Solution2();		
//		System.out.println(s.combinationSum4(new int[] {4,2,1},32));
		System.out.println(s.combinationSum4(new int[] {3,1,2,4},4));
	}
}
