package io.qinmi.leetcode.m.m0377;
import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
	递归回溯,实际上列出来了所有组合方案，貌似会超时，毕竟只是要一个方案个数
 */
public class Solution1 {
	public int[] select;
	public int count=0;
    public int combinationSum4(int[] candidates, int target) {    	
    	select=new int[target];
    	Arrays.sort(candidates);
    	putOneNum(candidates,0,target,0);
    	return count;
    }
    public void putOneNum(int[] candidates,int index,int target,int sum)
    {
    	if(sum==target)
		{
			// 求解成功，计数加1
			count++;
		}
    	if(sum>=target||index>target) return;//超出限制
    	for (int i = 0; i < candidates.length; i++)
		{// 尝试每一个数
			select[index] = candidates[i];
			if (sum + candidates[i] <= target)
				putOneNum(candidates, index + 1, target, sum + candidates[i]);
			else
				break;
		}
    }
    
	public static void main(String[] args) {
		Solution1 s = new Solution1();		
//		System.out.println(s.combinationSum4(new int[] {4,2,1},32));
		System.out.println(s.combinationSum4(new int[] {3,1,2,4},4));
	}
}
