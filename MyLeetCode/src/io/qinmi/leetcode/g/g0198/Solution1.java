package io.qinmi.leetcode.g.g0198;

import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
	用一个数组保存计数，面临一个屋子总有一种选择，就是抢这个就不能抢下一个，
	递归求解，用缓存减少运算次数。
 */
public class Solution1 {
//	private Map<Integer,Integer> map=new HashMap<Integer,Integer>();
	private int[] cache;
	private int[] nums;
    public int rob(int[] nums) {
    	cache=new int[nums.length];
    	Arrays.fill(cache, -1);
    	this.nums=nums;
        return rob(0);
    }
    public int rob(int start) {
    	if(start>=nums.length)
    		return 0;
    	if(start==nums.length-2)
    		return Math.max(nums[nums.length-1],nums[nums.length-2] );
    	if(start==nums.length-1)
    		return nums[nums.length-1];
        if(cache[start]>=0)
        	return cache[start];
        int max= Math.max(nums[start]+rob(start+2),nums[start+1]+rob(start+3));
        cache[start]=max;
        return max;
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.rob(new int[] {2,7,9,3,1}));
	}
}
