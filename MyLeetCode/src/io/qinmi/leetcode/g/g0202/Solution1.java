package io.qinmi.leetcode.g.g0202;

import java.util.HashSet;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
	循环实现判断，用Set来判断无限循环
 */
public class Solution1 {
	public Set<Integer> s=new HashSet<Integer>(); 
	
	
    public boolean isHappy(int n) {
    	while(n!=1)
    	{
    		if(s.contains(n))
    			return false;
    		s.add(n);
    		int temp=0;
    		while(n>0)
    		{
    			temp+=(n%10)*(n%10);
    			n=n/10;
    		}
    		n=temp;
    	}
    	return true;        
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.isHappy(19));
	}
}
