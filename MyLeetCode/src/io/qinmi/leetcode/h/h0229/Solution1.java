package io.qinmi.leetcode.h.h0229;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author NEO
 	解题思路：
	满足条件的数一定为0、1或者2个
 */
public class Solution1 {
    public List<Integer> majorityElement(int[] nums) {   		
    	int[] result=new int[2];
    	int count=0;
        Map<Integer,Integer> map=new HashMap <Integer,Integer>();
        int n=nums.length;
        for(int i:nums)
		{
			int c = 0;
			if (!map.containsKey(i))
				c = 1;
			else
				c = map.get(i)+1;
			map.put(i,c);
			if (c > n / 3) {
				result[count++] = i;
				map.put(i, -n);//避免重复计数count
			}
			if (count >= 2)
				break;
		}
    	List<Integer> list=new ArrayList<Integer>();
    	if(count>=1)
    		list.add(result[0]);
    	if(count>=2)
    		list.add(result[1]);
        return list;        
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.majorityElement(new int[] {1,1,2}));
//        Map<Integer,Integer> map=new HashMap <Integer,Integer>();
//        int c=map.get(1);
//        System.out.println(c);
	}
}
