package io.qinmi.leetcode.h.h0216;
import java.util.ArrayList;
import java.util.List;

/**
 * @author NEO
 	解题思路：
	递归回溯
 */
public class Solution1 {
	public int k=0;
	public int n=0;
	public int[] select;
	public List<List<Integer>> results;
    public List<List<Integer>> combinationSum3(int k, int n) {
    	results=new ArrayList<List<Integer>>();
    	if(k*(k-1)/2>n)return results;//k最大的情况是1+2+3+……+k=1/2*k(k-1)=n，不满足直接返回   	
    	this.n=n;
    	this.k=k;
    	select=new int[k];
    	putOneNum(0,1,0);
    	return results;
    }
    /**
     * @param index 选取第index个数字
     * @param c_i 最小选c_i
     * @param sum 已经得到的和
     */
    public void putOneNum(int index,int c_i,int sum)
    {
    	if(sum==n&&index==k)
		{
			// 求解成功，保存方案
			List<Integer> one = new ArrayList<Integer>();
			for (int i = 0; i < index; i++)
					one.add(select[i]);
			results.add(one);
		}
    	if(sum>=n||index>=k)return;//无论是选数还是一旦超过目标即可返回
    	//循环尝试c_i及以后的所有数，避免重复解，最大不超过9
    	for(int i=c_i;i<10;i++)
    	{
    		select[index]=i;
    		putOneNum(index+1,i+1,sum+i);
    	}

    }
    
	public static void main(String[] args) {
		Solution1 s = new Solution1();		
		System.out.println(s.combinationSum3(3,9));
	}
}
