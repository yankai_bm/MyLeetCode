package io.qinmi.leetcode.f.f0169;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NEO
 	解题思路：
	用一个map保存计数
 */
public class Solution1 {
    public int majorityElement(int[] nums) {
       Map<Integer,Integer> map=new HashMap <Integer,Integer>();
       int n=nums.length;
       for(int i:nums)
       {
    	   if(!map.containsKey(i))
    		   map.put(i,1);
    	   else
    		   if(map.put(i, map.get(i)+1)+1>n/2)
    			   return i;
       }
       return nums[0];
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.majorityElement(new int[] {2,2,1,1,1,2,2}));
	}
}
