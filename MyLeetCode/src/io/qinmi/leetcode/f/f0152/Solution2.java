package io.qinmi.leetcode.f.f0152;

/**
 * @author NEO
 	解题思路：
	将数组用0分割成为若干段，分别求最大值，之后再跟0比大小
	每一段看负数个数，如果个数为偶数，则全部相乘即可
	奇数个的话，有三种可能值，一种是第一个负数右侧的所有乘积，另一种是最后一个负数左侧所有值
	还有种是有且只有一个负数，则最大乘积就是负数本身
 */
/**
 * @author NEO
 *
 */
public class Solution2 {
    public int maxProduct(int[] nums) {
    	long max=Integer.MIN_VALUE;
    	int zero_index=-1;
    	int n=nums.length;
        for(int i=0;i<n;i++)
        {
        	if(nums[i]==0)
        	{
        		long cal=maxProduct(nums,zero_index+1,i);
        		if(cal>max)
        			max=cal;
        		zero_index=i;
        	}
        }
        long cal=maxProduct(nums,zero_index+1,n);
		if(cal>max)
			max=cal;
		if(zero_index!=-1)
			max=max<0?0:max;
        return (int) max;
    }
    
    /**
     * @param nums
     * @param start
     * @param end
     * @return 所有乘积
     */
    private int maxP(int[] nums,int start,int end)
    {
    	if(start>=end) return Integer.MIN_VALUE;
    	int cal=1;
        for(int i=start;i<end;i++)
        {
        	cal=cal*nums[i];
        }        
        return cal;
    }
    private int maxProduct(int[] nums,int start,int end)
    {
    	int count=0;
    	int first=-1,last=-1;
    	if(start>=end) return Integer.MIN_VALUE;
        for(int i=start;i<end;i++)
        {
        	if(nums[i]<0)
        	{
        		if(first<0)
        			first=i;
        		last=i;
        		count++;
        	}
        }
        if(count%2==0)
            return maxP(nums,start,end);
        else
        {
        	int left=Math.max(maxP(nums,start,last),nums[last]);
        	int right=Math.max(maxP(nums,first+1,end),nums[first]);
        	return Math.max(left,right);
        }
    }
    public static void main(String[] args) {
    	Solution2 s = new Solution2();		
//		System.out.println(s.maxProduct(new int[] {-2,1,-1,2,3,-1}));
//		System.out.println(s.maxProduct(new int[] {-1,0,-1}));
		System.out.println(s.maxProduct(new int[] {-1,-2,-3,0}));
	}
}
