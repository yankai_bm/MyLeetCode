package io.qinmi.leetcode.f.f0152;

/**
 * @author NEO
 	解题思路：
	逐个计算乘积，然后比较，遇到0直接短路
 */
public class Solution1 {
    public int maxProduct(int[] nums) {
    	long max=Integer.MIN_VALUE;
    	int n=nums.length;
        for(int i=0;i<n;i++)
        {
        	long cal=1;
        	for(int j=i;j<n;j++)
        	{
        		cal=cal*nums[j];
        		if(max<cal)
        			max=cal;
        		if(cal==0)
        			j=n;//遇到0直接短路
        	}
        }
        return (int) max;
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.maxProduct(new int[] {-1,0,-1}));
	}
}
