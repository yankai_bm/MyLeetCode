package io.qinmi.leetcode.c.c0090;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @author NEO
 	����˼·��
	�ݹ�
 */
public class Solution1 {

	private Integer[] nums;
	private int[] flag;
	private int len;
	private Map<Integer,Integer> count=new HashMap<Integer,Integer>();
	private List<List<Integer>> result= new ArrayList<List<Integer>>();
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        for(int i:nums)
        	if(count.containsKey(i))
        		count.put(i, count.get(i)+1);
        	else
        		count.put(i, 1);
        this.nums=count.keySet().toArray(new Integer[0]);

        len=this.nums.length;
        flag=new int[len];
    	list(0);
    	return result;
    }
    private void list(int depth)
    {
    	if(depth==len)
    	{
    		List<Integer> list=new ArrayList<Integer>();
    		for(int i=0;i<len;i++)
    			for(int j=0;j<flag[i];j++)
    				list.add(nums[i]);
    		result.add(list);
    	}
    	else
    	{
    		int max=count.get(nums[depth]);
			for(int j=max;j>=0;j--)
			{
				flag[depth]=j;
	    		list(depth+1);			
			}
    	}    	
    }
}
