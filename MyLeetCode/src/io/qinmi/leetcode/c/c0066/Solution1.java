package io.qinmi.leetcode.c.c0066;

/**
 * @author NEO
 	解题思路：
	诸位处理，需要考虑右对齐问题和进位问题，
 */
public class Solution1 {
    public int[] plusOne(int[] digits) {
    	int carry=1;//首位加一。之后都是进位了
    	int[] result=digits;
    	for(int i=digits.length-1;i>=0;i--)
		{
			int temp = digits[i] + carry;
			digits[i] = temp % 10;
			if (temp >= 10)
				carry = 1;
			else
				carry = 0;
		}
    	if(carry>0)
    	{
    		result=new int[digits.length+1];
    		System.arraycopy(digits, 0, result, 1, digits.length);
    		result[0]=carry;
    	}
    	return result;        
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.plusOne(new int[] {1,2,4}));
	}
}
