package io.qinmi.leetcode.c.c0067;

import java.math.BigInteger;

/**
 * @author NEO
 	解题思路：
	转换成long 再转回二进制string,会溢出
	只好改用BigInteger一行代码搞定
 */
public class Solution2 {
    public String addBinary1(String a, String b) {
    	Long ai=Long.parseLong(a, 2),bi=Long.parseLong(b, 2);
        long result=ai+bi;
        return Long.toBinaryString(result);
    }
    public String addBinary(String a, String b) {
        return new BigInteger(a,2).add(new BigInteger(b,2)).toString(2);
    }
    public static void main(String[] args) {
    	Solution2 s = new Solution2();		
    	
		System.out.println(s.addBinary("10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101",
				"110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011"));
	}
}
