package io.qinmi.leetcode.c.c0067;

/**
 * @author NEO
 	解题思路：
	诸位处理，需要考虑右对齐问题
 */
public class Solution1 {
    public String addBinary(String a, String b) {
        StringBuilder sb=new StringBuilder();
        int alen=a.length();
        int blen=b.length();
        int len=alen>blen?alen:blen;
        int carry=0;
        for(int i=0;i<len;i++)
        {
        	int va=(alen-1-i>=0)?(a.charAt(alen-1-i)-'0'):0;
        	int vb=(blen-1-i>=0)?(b.charAt(blen-1-i)-'0'):0;        	
        	sb.append((va+vb+carry)%2);
        	carry=(va+vb+carry)/2;
        }
        if(carry==1)sb.append(carry);
        return sb.reverse().toString();
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.addBinary("1010","1011"));
	}
}
