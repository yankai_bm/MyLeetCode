package io.qinmi.leetcode.c.c0077;
import java.util.ArrayList;
import java.util.List;
/**
 * @author NEO
 	解题思路：
	递归回溯
 */
public class Solution1 {
	public int n=0;
	public int k=0;
	public boolean[] select;
	public List<List<Integer>> results;
    /**
     * @param index 选择index这个数是否参加组合
     * @param count 已经选了count个数
     */
    public void putOneNum(int index,int count)
    {
    	if(count==k)
		{
			// 求解成功，保存方案
			List<Integer> one = new ArrayList<Integer>();
			for (int i = 0; i <index; i++)
				if(select[i])
					one.add(i+1);
			results.add(one);
			return;
		}
    	//选或者不选两种情况:
    	//选
    	if(index<n)
    	{
    	select[index]=true;
    	putOneNum(index+1,count+1);
    	//恢复现场
    	select[index]=false;
    	//不选，前提是后面的数还足够选
    	if(n-index-1>=k-count)
    		putOneNum(index+1,count);
    	}

    }
    public List<List<Integer>> combine(int n, int k) {
        this.n=n;
        this.k=k;
        results=new ArrayList<List<Integer>>();
        select=new boolean[n];//初始化为false
        putOneNum(0,0);
    	return results;
    }
	public static void main(String[] args) {
		Solution1 s = new Solution1();		
		System.out.println(s.combine(4,2));
	}
}
