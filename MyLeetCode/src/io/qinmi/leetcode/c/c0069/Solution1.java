package io.qinmi.leetcode.c.c0069;

/**
 * @author NEO
 	解题思路：
	简单朴素的想法，找到r使得r*r<=x (r+1)*(r+1)>x即可,通过long规避int*int溢出的问题，46340
 */
public class Solution1 {
    public int mySqrt(int x) {
//        return (int) Math.sqrt(x);
    	if(x<=1)return x;
    	long result=0;
    	while(result*result<=x)
    	{
    		if((result+1)*(result+1)>x)
    			break;
    		result++;
    	}
    	return (int) result;
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.mySqrt(2147395600));
	}
}
