package io.qinmi.leetcode.c.c0069;

/**
 * @author NEO
 	解题思路：
	牛顿迭代法
 */
public class Solution2 {
    public int mySqrt(int x) {
//        return (int) Math.sqrt(x);
    	if(x<=1)return x;
    	long result=x;
    	while(x/result<result)
    	    result= (result + x / result)/2;
    	return (int) result;
    }
    public static void main(String[] args) {
    	Solution2 s = new Solution2();		
		System.out.println(s.mySqrt(9));
	}
}
