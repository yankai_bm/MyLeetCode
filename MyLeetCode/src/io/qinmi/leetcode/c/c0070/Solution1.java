package io.qinmi.leetcode.c.c0070;
import java.util.HashMap;
import java.util.Map;
/**
 * @author NEO
 	解题思路：
	考虑递归的方式，通过Map缓存减少重复计算
 */
public class Solution1 {
	public Map<Integer,Integer> map=new HashMap<Integer,Integer>();
    public int climbStairs(int n) {
        if(n<=2)return n;
        Integer n1=ensure(n-1);
        Integer n2=ensure(n-2);
        return n1+n2;
    }
    public int ensure(int n)
    {
    	Integer n1=map.get(n);
    	if(n1==null)
    	{
    		n1=climbStairs(n);
    		map.put(n, n1);
    	}
    	return n1;
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.climbStairs(4));
	}
}
