package io.qinmi.leetcode.o.o0459;

/**
 * @author NEO
 	解题思路：
	因数分解
 */
public class Solution1 {
    public boolean repeated(String s,int len)
	{
		char[] chars = s.toCharArray();
		for (int i = 0; i < s.length(); i++)
			if (chars[i] != chars[i % len])
				return false;
		return true;
	}
    public boolean repeatedSubstringPattern(String s) {
        int l=s.length();
        for(int i=l/2;i>=1;i--)
        	if(l%i==0&&repeated(s,i))        	
        		return true;
        return false;
    }
	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(2%1);
		System.out.println(s.repeatedSubstringPattern("abab"));
	}
}
