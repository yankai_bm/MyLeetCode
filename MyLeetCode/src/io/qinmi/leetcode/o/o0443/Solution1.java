package io.qinmi.leetcode.o.o0443;

/**
 * @author NEO
 	解题思路：
 	两个指针分别控制读和写
 */
public class Solution1 {
	public int compress(char[] chars) {
		int writer = 0, reader = 0, count = 1;
		char curr = (char) 34;// 初始化为范围外避免影响
		for (; reader < chars.length; reader++) {
			if (chars[reader] == curr)
				count++;
			else if (curr == 34)
				curr = chars[reader];
			else {
				chars[writer++] = curr;
				writer = write(chars, count, writer);
				curr = chars[reader];
				count = 1;
			}
		}
		if (chars.length > 0) {
			chars[writer++] = curr;
			writer = write(chars, count, writer);
		}
		return writer;
	}
    public int write(char[] chars,int count,int writer)
    {
    	if(count>1)
    	{
    		if(count>=1000)
    			chars[writer++]=(char) (count/1000+'0');
    		if(count>=100)
    			chars[writer++]=(char) ((count/100)%10+'0');
    		if(count>=10)
    			chars[writer++]=(char) ((count/10)%10+'0');
    		chars[writer++]=(char) (count%10+'0');    		
    	}
    	return writer;
    }
	public static void main(String[] args) {
		Solution1 s = new Solution1();
		char[] chars=new char[] {'o','o','o','o','o','o','o','o','o','o'};
		System.out.println(s.compress(chars));
		System.out.println(chars);
	}
}
