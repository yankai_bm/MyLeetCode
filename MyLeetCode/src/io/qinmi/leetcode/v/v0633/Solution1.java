package io.qinmi.leetcode.v.v0633;

/**
 * @author NEO
 	解题思路：
	简单朴素的想法，找到1-sqrt(c)中的r使得c-r*r为完全平方数
 */
public class Solution1 {
    public boolean judgeSquareSum(int c) {
        int s1=(int)Math.sqrt(c);
        int s2=(int)Math.sqrt(c/2);
        for(int i=s1;i>=s2;i--)
        {
        	int temp=c-i*i;
        	if((int)Math.sqrt(temp)*(int)Math.sqrt(temp)==temp)
        		return true;
        }
        return false;      		
    }

    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.judgeSquareSum(15));
	}
}
