package io.qinmi.leetcode.i.i0258;

/**
 * @author NEO
 	解题思路：
	所有9的倍数，字面数字加起来都是9的倍数
 */
public class Solution2 {
    public int addDigits(int num) {
    	return (num-1)%9+1;
    }
    public static void main(String[] args) {

    }
}
