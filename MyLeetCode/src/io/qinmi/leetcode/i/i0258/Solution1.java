package io.qinmi.leetcode.i.i0258;

/**
 * @author NEO
 	解题思路：
	递归实现
 */
public class Solution1 {
    public int addDigits(int num) {
        if(num<10) return num;
        int t=0;
        while(num!=0)
        {
        	t+=num%10;
        	num=num/10;
        }
        return addDigits(t);
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.addDigits(38));
	}
}
