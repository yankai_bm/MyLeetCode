package io.qinmi.leetcode.i.i0264;

/**
 * @author NEO
 	解题思路：
 	丑数都是正整数
 */
public class Solution1 {
    public boolean isUgly(int num) {
        if(num<=0) return false;
        while(num%2==0)
        	num=num/2;//排除因子2
        while(num%3==0)
        	num=num/3;//排除因子3
        while(num%5==0)
        	num=num/5;//排除因子5
        return num==1;//等于1的或者没有其他因子
    }
    public int nthUglyNumber(int n) {
    	int count=0;
    	int i=0;
    	while(count<n)
    		if(isUgly(++i))
    			count++;
    	return i;
    }
	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.nthUglyNumber(10));
	}
}
