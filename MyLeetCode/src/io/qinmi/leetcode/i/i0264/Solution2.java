package io.qinmi.leetcode.i.i0264;

import java.util.ArrayList;
import java.util.List;

/**
 * @author NEO
 	解题思路：
 	丑数都是由另一个丑数乘以2或3或5得到的
 */
public class Solution2 {
    public int nthUglyNumber(int n) {
    	List<Integer> nums= new ArrayList<Integer>();
    	nums.add(1);
    	int a=0,b=0,c=0;
    	int result=nums.get(nums.size()-1);
    	
    	while(--n>0)
    	{
    		while(nums.get(a)*2<=result)
    			a++;
    		while(nums.get(b)*3<=result)
    			b++;
    		while(nums.get(c)*5<=result)
    			c++;
    		int na=nums.get(a)*2;
    		int nb=nums.get(b)*3;
    		int nc=nums.get(c)*5;
    		if(na<=nb&&na<=nc)
    			nums.add(na);
    		else if(nb<=na&&nb<=nc)
    			nums.add(nb);
    		else if(nc<=nb&&nc<=na)
    			nums.add(nc);
    		result=nums.get(nums.size()-1);
    	}
    	return result;
    }
	public static void main(String[] args) {
		Solution2 s = new Solution2();
		System.out.println(s.nthUglyNumber(10));
	}
}
