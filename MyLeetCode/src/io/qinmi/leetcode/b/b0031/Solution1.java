package io.qinmi.leetcode.b.b0031;

import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
	自右向左，找到第一次递减，例如i-1<i，然后用i到最右中最小的比i-1大的数与i-1交换，然后对i到最右排序递增排序
	例如 找到 2 4 3 2 1的子序列，交换3 4 2 2 1，排序为3 1 2 2 4即可
 */
public class Solution1 {
	public void nextPermutation(int[] nums) {
		for (int i = nums.length - 1; i >= 0; i--)
			if (i == 0||nums[i] > nums[i - 1] ) {
				if (i > 0) {
					int minIndex=i;
					for (int j = i; j< nums.length; j++)
						if(nums[j]>nums[i-1]&&nums[j]<nums[minIndex])
							minIndex=j;
					int temp = nums[i - 1];
					nums[i - 1] = nums[minIndex];
					nums[minIndex] = temp;
				}
				Arrays.sort(nums, i, nums.length);
				break;
			}
	}

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		s.nextPermutation(new int [] {1,3,2});
	}
}
