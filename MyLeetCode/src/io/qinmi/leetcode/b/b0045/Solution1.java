package io.qinmi.leetcode.b.b0045;

import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
	如果到达某一点i可在k步到达，则从这一点之后的i+1到i+nums[i]点一定不超过k+1;
 */
public class Solution1 {
	private int[] min_step;
    public int jump(int[] nums) {
    	min_step=new int[nums.length];
    	Arrays.fill(min_step, Integer.MAX_VALUE);
    	min_step[0]=0;
    	for(int i=0;i<nums.length;i++)
    	{
    		for(int j=i+1;j<nums.length&&j<=i+nums[i];j++)
    		{
    			if(min_step[i]+1<min_step[j])
    				min_step[j]=min_step[i]+1;
    			if(j==nums.length-1)
    				break;
    		}
    	}
    	return min_step[nums.length-1];
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.jump(new int[] {2,3,1,1,4}));
	}
}
