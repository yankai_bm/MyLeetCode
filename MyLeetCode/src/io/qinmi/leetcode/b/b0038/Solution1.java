package io.qinmi.leetcode.b.b0038;

/**
 * @author NEO
 	解题思路：
	读懂题目，注意最后一个词在循环外补上
 */
public class Solution1 {
    public String countAndSay(int n) {
        String word="1";
        for(int i=1;i<n;i++)
        {
        	StringBuilder sb=new StringBuilder();
        	char count_char=' ';
        	int count=0;
            for(int j=0;j<word.length();j++)
            {
            	if(word.charAt(j)==count_char)
            		count++;
            	else
            	{
            		if(count_char!=' ')
            		{
            			sb.append(count).append(count_char);
            		}
            		count_char=word.charAt(j);
            		count=1;
            	}
            }
            sb.append(count).append(count_char);
            word=sb.toString();
        }
        return word;
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();		
		System.out.println(s.countAndSay(5));
	}
}
