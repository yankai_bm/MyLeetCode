package io.qinmi.leetcode.b.b0039;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
/**
 * @author NEO
 	解题思路：
	递归回溯
 */
public class Solution1 {
	public int target=0;
	public int[] candidates;
	public int[] select;
	public 	Comparator<? super Integer> c;
	public List<List<Integer>> results;
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
    	this.target=target;
    	results=new ArrayList<List<Integer>>();
    	if(candidates.length==0) return results;
    	this.candidates=candidates;
    	Arrays.sort(candidates);//从小到大选数，从而避免重复方案
    	select=new int[target/candidates[0]];//无论是否整除最多也只需要这么多元素
    	putOneNum(0,0,0);
    	return results;
    }
    public void putOneNum(int index,int c_i,int sum)
    {
    	if(sum==target)
		{
			// 求解成功，保存方案
			List<Integer> one = new ArrayList<Integer>();
			for (int i = 0; i < index; i++)
					one.add(select[i]);
			results.add(one);
		}
    	if(sum>=target||index>=select.length) return;//正整数，一旦超过目标即可返回
    	//循环尝试c_i及以后的所有数，避免重复解
    	for(int i=c_i;i<candidates.length;i++)
    	{
    		select[index]=candidates[i];
    		putOneNum(index+1,i,sum+candidates[i]);
    	}

    }
    
	public static void main(String[] args) {
		Solution1 s = new Solution1();		
		System.out.println(s.combinationSum(new int[] {2,3,5},8));
	}
}
