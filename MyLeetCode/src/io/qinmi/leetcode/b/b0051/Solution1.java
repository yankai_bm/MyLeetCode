package io.qinmi.leetcode.b.b0051;

import java.util.ArrayList;
import java.util.List;

/**
 * @author NEO
 	解题思路：
	递归回溯遍历 动态规划
 */
public class Solution1 {
	//记录每个纵向是否有皇后，取值从0到n-1
	public boolean[] vertical;
	//自右上角斜向左下角的向左下的斜线,共2n-1条，包括两条仅有一格的斜线，每条斜线的特征是坐标和一样，取值从0到2n-2
	public boolean[] left;
	//自左上角斜向右下角的向右下的斜线，,共2n-1条，包括两条仅有一格的斜线，每条斜线的特征是坐标和一样，取值从1-n到n-1，为了方便计算，坐标再加n-1
	public boolean[] right;
	//保存棋盘，方便回溯
	public char[][] board;
	//返回方案
	public List<List<String>> results=new ArrayList<List<String>>();
    public List<List<String>> solveNQueens(int n) {
    	vertical=new boolean[n];//初始化为false
    	left=new boolean[2*n-1];//初始化为false
    	right=new boolean[2*n-1];//初始化为false
    	board=new char[n][n];
    	initBoard();//初始化为全空棋盘
    	putOneQueen(n,0);
    	return results;
    }
    public void putOneQueen(int n,int index)
	{
		if (index == n) {// 求解成功，保存方案
			List<String> one = new ArrayList<String>();
			for (int i = 0; i < n; i++)
				one.add(new String(board[i]));
			results.add(one);
		} else
			for (int i = 0; i < n; i++) {
				if (!vertical[i])// 纵向没有皇后，横向本身通过index已经隔离
					if (!left[i + index])// 东北西南方向没有皇后
						if (!right[i - index + n - 1])// 西北东南方向没有皇后
						{
							vertical[i] = true;
							left[i + index] = true;
							right[i - index + n - 1] = true;
							board[index][i] = 'Q';
							//递归下一行的皇后
							putOneQueen(n, index + 1);
							// 恢复现场
							vertical[i] = false;
							left[i + index] = false;
							right[i - index + n - 1] = false;
							board[index][i] = '.';
						}
			}
	}
    public void initBoard()
    {
    	for(int i=0;i<board.length;i++)
    		for(int j=0;j<board[i].length;j++)
    			board[i][j]='.';
    }
	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.solveNQueens(4));
	}
}
