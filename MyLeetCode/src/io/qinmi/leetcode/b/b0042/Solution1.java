package io.qinmi.leetcode.b.b0042;

/**
 * @author NEO
 	解题思路：
	横向考虑水位高度和填充看起来有点复杂，纵向切分会简单一些
 */
public class Solution1 {
    public int trap(int[] height) {
    	//找到最高水位线，分层存水以此为终点
       int max_height=0;
       for(int n=0;n<height.length;n++)
    	   if(height[n]>max_height)
    		   max_height=height[n];
       int water=0;
       for(int h=1;h<=max_height;h++)
       {
           int start=-1;
           for(int i=0;i<height.length;i++)
           {
        	   if(height[i]>=h)
        	   {
        		   //起始部分存不住水，
        		   if(start>=0&&i-start>=2)
        		   {
        			   water=water+i-start-1;
        		   }
        		   start=i;
        	   }
        	   
           }

       }
       return water;
    }
	public static void main(String[] args) {
		Solution1 s = new Solution1();		
		System.out.println(s.trap(new int[] {0,1,0,2,1,0,1,3,2,1,2,1}));
	}
}
