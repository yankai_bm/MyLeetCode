package io.qinmi.leetcode.b.b0042;

/**
 * @author NEO
 	解题思路：
	效率高一点的算法，首先确定存水的区间，找所有的阶段高点，然后根据阶段高点
 */
public class Solution2 {
	public int trap(int[] height) {
		final int length = height.length;

		if (length<=2) return 0;
		int start = length;// 找到左侧第一个局部高点，在此点以左不会积水
		for (int h = 0; h < length - 1; h++)
			if (height[h] >height[h + 1]) {
				start = h;
				break;
			}
		int end = 0;// 找到右侧第一个局部高点，在此点以右不会积水
		for (int h = length - 1; h > 0; h--)
			if (height[h - 1] <height[h]) {
				end = h;
				break;
			}
		if (end < start + 2)
			return 0;
		int all_water = trap(height, start, end);
		return all_water;
	}
    public int trap(int[] height,int start,int end)
	{
		int water = 0;
		int left = start;
		int right = 0;
		while (right != end) {
			for (int h = left+1; h <= end; h++)
				if (h == end || (height[h] >= height[h + 1]&&height[h] > height[h- 1])) {
					right = h;
					break;
				}
			int water_line = height[left] > height[right] ? height[right] : height[left];
			if (right >= left + 2)
				for (int h = left + 1; h <= right - 1; h++) {
					if (water_line > height[h]) {
						water += (water_line - height[h]);
						height[h] = water_line;
					}
				}
			if(water_line==height[left])
				left=right;
		}
		return water;
	}
	public static void main(String[] args) {
		Solution2 s = new Solution2();		
		System.out.println(s.trap(new int[] {5,4,1,2}));
	}
}
