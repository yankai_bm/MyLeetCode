package io.qinmi.leetcode.b.b0033;

/**
 * @author NEO
 	解题思路：
	考虑到先用二分查法找到旋转点，正常来说是递增序列，所以旋转点以左都大于旋转点以右，且旋转点两侧本身也都是递增序列
	假设数组为0到l，找到旋转点假设为r，则排序顺序为r到l，0到r-1,于是存在一个map函数把排序序号0到l-1映射到以上的空间中
 */
public class Solution1 {
	public int search(int[] nums, int target) {
		if (nums.length == 0)
			return -1;
		int r = findR(nums);
		int l = nums.length;
		int left = 0;
		int right = l - 1;
		while (left < right) {
			int mid = (left + right) / 2;
			if (target == nums[indexMap(r, l, mid)])
				return indexMap(r, l, mid);
			else if (target > nums[indexMap(r, l, mid)])
				left = mid + 1;
			else
				right = mid - 1;
		}
		int index = indexMap(r, l, right);
		if (index > l || index < 0 || nums[index] != target)
			return -1;
		else
			return index;
	}
    public int findR(int[] nums) {
    	int left=0;
    	int right=nums.length-1;
    	while(left<right)
    	{
            int mid = (left+right)/2;
            if(nums[mid]>nums[right])
            	left=mid+1;
            else
            	right=mid;
    	}    			
		return left;        
    }
    int indexMap(int r,int l,int i)
    {
    	return (i+r)%l;
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();
//		System.out.println(s.indexMap(4,7,4));
//		System.out.println(s.findR(new int [] {4,5,6,7,0,1,2}));
		System.out.println(s.search(new int [] {4,5,6,7,0,1,2},0));
		System.out.println(s.search(new int [] {4,5,6,7,0,1,2},3));
	}
}
