package io.qinmi.leetcode.b.b0040;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
/**
 * @author NEO
 	解题思路：
	递归回溯，set排除重复，可以尝试排序排重的方式
 */
public class Solution1 {
	public Set<String> set=new HashSet<String>();
	public boolean[] select;
	public 	Comparator<? super Integer> c;
	public List<List<Integer>> results;
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
    	results=new ArrayList<List<Integer>>();
    	select=new boolean[candidates.length];//初始化为false
    	putOneNum(candidates,0,target,0);
    	return results;
    }
    public void putOneNum(int[] candidates,int index,int target,int sum)
    {
    	if(sum==target)
		{
			// 求解成功，保存方案
			List<Integer> one = new ArrayList<Integer>();
			for (int i = 0; i < candidates.length; i++)
				if(select[i])
					one.add(candidates[i]);
			one.sort(c);
			StringBuilder sb=new StringBuilder();
			for(Integer i:one)
				sb.append(i).append('-');
			String s=sb.toString();
			if(!set.contains(s))
			{
				results.add(one);
				set.add(s);
			}
		}
    	if(sum>=target) return;//正整数，一旦超过目标即可返回
    	if(index<candidates.length)
    	{//尝试两种情况，选这个数与不选这个数
    		select[index]=true;
    		putOneNum(candidates,index+1,target,sum+candidates[index]);
    		select[index]=false;
    		putOneNum(candidates,index+1,target,sum);
    	}
    }
    
	public static void main(String[] args) {
		Solution1 s = new Solution1();		
		System.out.println(s.combinationSum2(new int[] {10,1,2,7,6,1,5},8));
	}
}
