package io.qinmi.leetcode.b.b0058;

/**
 * @author NEO
 	解题思路：
	逆序遍历，
 */
public class Solution2 {
	public int lengthOfLastWord(String s) {
		int len = 0;
		for (int i = s.length()-1; i >=0; i--) 
			if (' ' != s.charAt(i)) 
				len++;
			 else if(len>0)
				break;
		return len;
	}

	public static void main(String[] args) {
		Solution2 s = new Solution2();
		System.out.println(s.lengthOfLastWord("Hello World"));
	}
}
