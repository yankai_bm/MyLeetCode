package io.qinmi.leetcode.b.b0058;

/**
 * @author NEO
 	解题思路：
	顺序遍历，两个数值来保存单词长度
 */
public class Solution1 {
	public int lengthOfLastWord(String s) {
		int len = 0;
		int last_len = 0;
		for (int i = 0; i < s.length(); i++) {
			if (' ' == s.charAt(i)) {
				last_len = len > 0 ? len : last_len;
				len = 0;
			} else 
				len++;
		}
		return len == 0 ? last_len : len;
	}

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.lengthOfLastWord("Hello World"));
	}
}
