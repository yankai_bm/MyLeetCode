package io.qinmi.leetcode.b.b0060;

/**
 * @author NEO
 	解题思路：
	字典排序，直到找到第k个排列
 */
public class Solution1 {
	private int n,k,count; 
	private char[] chars;
	private boolean[] flag;
	private String result;
    public String getPermutation(int n, int k) {
    	this.n=n;
    	this.k=k;
    	count=0;
    	chars=new char[n];
    	flag=new boolean[n];
    	find(0);
    	return result;
    }
    private void find(int index)
    {
    	if(result!=null) return;
    	if(index==n) {
    		if(++count==k)
    			result=new String(chars);    		
    		return;
    	}
    	for(int i=0;i<n;i++)
    	{
    		if(!flag[i])
    		{
    			flag[i]=true;
    			chars[index]=(char) ('0'+i+1);
    			find(index+1);
    			flag[i]=false;
    		}
    	}
    	
    }
    
	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.getPermutation(4,9));
	}
}
