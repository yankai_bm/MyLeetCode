package io.qinmi.leetcode.b.b0055;

import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
	考虑到如果存在不能到达的情况，则一定在某点出现0，而且在这个点之前的所有点都无法越过这个点
 */
public class Solution1 {
	private int[] max_reach;
    public boolean canJump(int[] nums) {
    	max_reach=new int[nums.length];
//    	Arrays.fill(max_reach, -1);
    	for(int i=0;i<nums.length;i++)
    	{
    		if(i+nums[i]>max_reach[i])
    			max_reach[i]=i+nums[i];
    		for(int j=i;j<nums.length&&j<=i+nums[i];j++)
    		{    			
    			if(max_reach[i]>max_reach[j])
    				max_reach[j]=max_reach[i];    			
    		}    		
    		if(max_reach[i]>=nums.length-1)
    			return true;
    		if(max_reach[i]==i)
    			return false;
    	}
    	return true;
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.canJump(new int[] {3,2,1,0,4}));
	}
}
