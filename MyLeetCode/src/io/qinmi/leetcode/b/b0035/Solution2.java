package io.qinmi.leetcode.b.b0035;

/**
 * @author NEO
 	解题思路：
	二分查找，无重复元素是一个简化条件
 */
public class Solution2 {
    public int searchInsert(int[] nums, int target) {
    	int start=0,end=nums.length-1;
    	if(target<=nums[start])return 0;
    	if(target>=nums[end])return target>nums[end]?end+1:end;
    	while(start<end)
    	{
    		int index=(end+start)/2;
        	if(target==nums[index])return index;
        	if(target>nums[index])start=index;
        	if(target<nums[index])end=index;
    		if(start==end-1)return end;
    	}
        return nums.length;
    }
}
