package io.qinmi.leetcode.b.b0035;

/**
 * @author NEO
 	解题思路：
	暴力搜索，代码很简单
 */
public class Solution1 {
    public int searchInsert(int[] nums, int target) {
        for(int i=0;i<nums.length;i++)
        	if(nums[i]>=target)return i;
        return nums.length;
    }
}
