package io.qinmi.leetcode.b.b0047;
import java.util.*;
/**
 * @author NEO
 	解题思路：
	递归。通过Set来排除重复
 */
public class Solution1 {
	public Set<String> set=new HashSet<String>();
	public List<List<Integer>> list=new ArrayList<List<Integer>>();
	public int[] result;
	public boolean[] flags;
	public int[] nums;
    public List<List<Integer>> permuteUnique(int[] nums) {
    	this.nums=nums;
    	result=new int[nums.length];
    	flags=new boolean[nums.length];
    	permute(0);
    	return list;
    }
    public void permute(int depth) {
    	if(depth==nums.length)
    	{
    		List<Integer> one=new ArrayList<Integer>();
    		StringBuilder sb=new StringBuilder();
    		for(int i=0;i<nums.length;i++)
    		{
    			one.add(result[i]);
    			sb.append(result[i]).append('|');
    		}
    		String key=sb.toString();
    		if(!set.contains(key))
			{
    			set.add(key);
				list.add(one);
			}
    	}
    	else
    	{
    		for(int i=0;i<nums.length;i++)
    		{
    			if(!flags[i])
    			{
    				result[depth]=nums[i];
    				flags[i]=true;
    				permute(depth+1);
    				flags[i]=false;
    			}
    		}
    	}
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.permuteUnique(new int [] {}));
	}
}
