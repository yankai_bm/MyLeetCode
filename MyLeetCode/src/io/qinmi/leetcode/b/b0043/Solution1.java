package io.qinmi.leetcode.b.b0043;

/**
 * @author NEO
 	解题思路：
	模拟竖式计算乘法过程，先个位数相乘，再加起来，注意进位和位移
 */
public class Solution1 {
    public String multiply(String a, String b) {
        StringBuilder zeros=new StringBuilder();
        int alen=a.length();
        int blen=b.length();
        String result="0";
        //用a的每一位去乘b
        for(int i=alen-1;i>=0;i--)
        {
        	char a_char=a.charAt(i);
            int carry=0;//进位
            StringBuilder sb=new StringBuilder();
        	//分别处理b的每一位
            for(int j=blen-1;j>=0;j--)
            {
            	int temp=(a_char-'0')*(b.charAt(j)-'0')+carry;
            	carry=temp/10;
            	sb.append(temp%10);
            }
            if(carry>0)sb.append(carry);
            sb.reverse();
            sb.append(zeros);
            result=addDecimal(sb.toString(),result);
            zeros.append(0);
        }
        return removeZeros(result);
    }
    public String addDecimal (String a, String b)
    {
        StringBuilder sb=new StringBuilder();
        int alen=a.length();
        int blen=b.length();
        int len=alen>blen?alen:blen;
        int carry=0;
        for(int i=0;i<len;i++)
        {
        	int va=(alen-1-i>=0)?(a.charAt(alen-1-i)-'0'):0;
        	int vb=(blen-1-i>=0)?(b.charAt(blen-1-i)-'0'):0;        	
        	sb.append((va+vb+carry)%10);
        	carry=(va+vb+carry)/10;
        }
        if(carry>0)sb.append(carry);
        return sb.reverse().toString();
    }

    public String removeZeros(String a)
    {//移除所有左侧的0
    	int i=0;
    	for(;i<a.length();i++)
    		if(a.charAt(i)!='0')
    			break;
    	if(i==a.length())
    		return a.substring(a.length()-1);
    	else
    		return a.substring(i);
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.multiply("2","3"));
	}
}
