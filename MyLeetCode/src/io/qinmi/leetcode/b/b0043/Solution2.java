package io.qinmi.leetcode.b.b0043;

import java.math.BigInteger;

/**
 * @author NEO
 	解题思路：
	禁止使用的BigInteger一行代码搞定
 */
public class Solution2 {
    public String multiply(String num1, String num2) {
        return new BigInteger(num1).multiply(new BigInteger(num2)).toString();
    }

    public static void main(String[] args) {
    	Solution2 s = new Solution2();    	
		System.out.println(s.multiply("10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101",
				"110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011"));
	}
}
