package io.qinmi.leetcode.b.b0046;
import java.util.*;
/**
 * @author NEO
 	����˼·��
	�ݹ�
 */
public class Solution1 {
	public int[] result;
	public List<List<Integer>> list;
	public boolean[] flags;
	public int[] nums;
    public List<List<Integer>> permute(int[] nums) {
    	list=new ArrayList<List<Integer>>();
    	this.nums=nums;
    	result=new int[nums.length];
    	flags=new boolean[nums.length];
    	permute(0);
    	return list;
    }
    public void permute(int depth) {
    	if(depth==nums.length)
    	{
    		List<Integer> one=new ArrayList<Integer>();
    		for(int i=0;i<nums.length;i++)
    			one.add(result[i]);
    		list.add(one);    		
    	}
    	else
    	{
    		for(int i=0;i<nums.length;i++)
    		{
    			if(!flags[i])
    			{
    				result[depth]=nums[i];
    				flags[i]=true;
    				permute(depth+1);
    				flags[i]=false;
    			}
    		}
    	}
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.permute(new int [] {}));
	}
}
