package io.qinmi.leetcode.a.a0027;

/**
 * @author NEO
 	解题思路：
	从两侧两个指针，左侧一个是判断指针，右侧一个是填充指针
 */
public class Solution1 {
    public int removeElement(int[] nums, int val) {
        int left=0,right=nums.length-1,len=nums.length;
        if(len==0) return 0;
		while (nums[right] == val) {
			right--;
			len--;
			if(left>right)	return 0;
		}
        while(left<right)
		{
			if (nums[left] == val) {
				nums[left] = nums[right];
				nums[right] = val;
				while (nums[right] == val) {
					right--;
					len--;
					if(left>=right)	break;
				}
			}
			left++;
		}
        return len;        
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.removeElement(new int [] {4,5},5));
	}
}
