package io.qinmi.leetcode.a.a0028;

/**
 * @author NEO
 	解题思路：
	先判断第一个字符可以大幅减少对象创建，从而提高效率
 */
public class Solution1 {
	public int strStr(String haystack, String needle) {
		int len=needle.length();
		if(len==0)	return 0;
		int firstChar=needle.charAt(0);
		for(int i=0;i<haystack.length()-len+1;i++)
		{
			if(haystack.charAt(i)==firstChar&&needle.equals(haystack.substring(i, i+len)))
				return i; 
		}
		return -1;
	}

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.strStr("mississippi", "pi"));
//		"mississippi"
//		"pi"
	}
}
