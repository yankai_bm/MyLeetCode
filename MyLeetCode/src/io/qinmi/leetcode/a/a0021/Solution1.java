package io.qinmi.leetcode.a.a0021;

/**
 * @author NEO
 	解题思路：
	很简单的归并，加个哑节点代码简单很多
 */
public class Solution1 {
	public class ListNode {
		int val;
		public ListNode getNext() {
			return next;
		}
		public void setNext(ListNode next) {
			this.next = next;
		}
		ListNode next;
		ListNode(int x) {
			val = x;
		}
	}

	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		ListNode dummy=new ListNode(0);
		ListNode current=dummy;
		while(l1!=null&&l2!=null)
		{

			if (l1.val > l2.val) {
				current.next = l2;
				current = l2;
				l2 = l2.next;
			} else {
				current.next = l1;
				current = l1;
				l1 = l1.next;
			}
		}
		if (l1 == null) {
			current.next = l2;
		}
		if (l2 == null) {
			current.next = l1;
		}
		return dummy.next;
	}

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		ListNode l0=s.new ListNode(1);
		ListNode l1=s.new ListNode(2);
		l0.setNext(l1);
		ListNode l2=s.new ListNode(4);
		l1.setNext(l2);

		ListNode k0=s.new ListNode(1);
		ListNode k1=s.new ListNode(3);
		k0.setNext(k1);
		ListNode k2=s.new ListNode(4);
		k1.setNext(k2);
		s.mergeTwoLists(l0, k0);
	}
}
