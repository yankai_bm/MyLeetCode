package io.qinmi.leetcode.a.a0018;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
	双重循环，之后双指针
 */
public class Solution1 {

	private Set<String> set=new HashSet<String>();
	public boolean exist(int a,int b,int c)
	{
		String s=new StringBuilder().append(a).append(',').append(b).append(',').append(c).toString();
		if(set.contains(s.toString()))
			return true;
		else
		{
			set.add(s);
			return false;
		}
	}
	public List<List<Integer>> fourSum(int[] nums, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		Arrays.sort(nums);		
		for (int i = 0; i < nums.length - 3; i++)
		{
			for (int l = i + 1; l < nums.length - 2; l++) {
				int sum = target - nums[i]- nums[l];
				int j = l + 1, k = nums.length - 1;
				while (j < k) {
					if (nums[j] + nums[k] == sum && !exist(nums[i], nums[l], nums[j])) {
						List<Integer> list = new ArrayList<Integer>();
						list.add(nums[i]);
						list.add(nums[l]);
						list.add(nums[j]);
						list.add(nums[k]);
						result.add(list);
					} else if (sum < nums[j] + nums[k])
						k--;
					else
						j++;
				}
			}

		}
		return result;
	}
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.fourSum(new int [] {1, 0, -1, 0, -2, 2},0));
	}
}
