package io.qinmi.leetcode.a.a0004;

/**
 * @author NEO
 	解题思路：
 	先归并，再求中位数，时间复杂度O（m+n）
 */
public class Solution1 {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
    	int[] nums=new int[nums1.length+nums2.length];
    	int i=0,j=0,k=0;
    	while(k<nums.length)
    	{
    		k++;    		
    		if(i>=nums1.length)
    		{
    			nums[k-1]=nums2[j++];
    			continue;
    		}
    		if(j>=nums2.length)
    		{
    			nums[k-1]=nums1[i++];
    			continue;
    		}
    		if(nums2[j]<nums1[i])
    			nums[k-1]=nums2[j++];
    		else 
    			nums[k-1]=nums1[i++];		
    	}
    	if(k%2==0)
    	{
    		return ((double)nums[k/2]+(double)nums[k/2-1])/2;
    	}
    	else
    	{

        	return nums[k/2];
    	}
    		
    }


	public static void main(String[] args) {
		int[] nums1 = {1, 3};
		int[] nums2 = {2};
		System.out.println(new Solution1().findMedianSortedArrays(nums1,nums2));
	}

}
