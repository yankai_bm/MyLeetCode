package io.qinmi.leetcode.a.a0013;

/**
 * @author NEO
 	解题思路：
	确定每个元素的值，再确定正负号
 */
public class Solution1 {
    public int romanToInt(String s) {
    	int result=0;
    	for(int i=0;i<s.length();i++)
    	{
    		if(s.charAt(i)=='M')
    			result+=1000;
    		if(s.charAt(i)=='D')
    			result+=500;
    		if(s.charAt(i)=='L')
    			result+=50;
    		if(s.charAt(i)=='V')
    			result+=5;
    		if(s.charAt(i)=='C')
    		{
    			if(i<s.length()-1&&(s.charAt(i+1)=='M'||s.charAt(i+1)=='D'))
    				result-=100;
    			else
    				result+=100;
    		}
    		if(s.charAt(i)=='X')
    		{
    			if(i<s.length()-1&&(s.charAt(i+1)=='L'||s.charAt(i+1)=='C'))
    				result-=10;
    			else
    				result+=10;
    		}
    		if(s.charAt(i)=='I')
    		{
    			if(i<s.length()-1&&(s.charAt(i+1)=='X'||s.charAt(i+1)=='V'))
    				result-=1;
    			else
    				result+=1;
    		}
    	}
        return result;
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.romanToInt("MCMXCIV"));
	}

}
