package io.qinmi.leetcode.a.a0014;

/**
 * @author NEO
 	解题思路：
	暴力搜索
 */
public class Solution1 {
    public String longestCommonPrefix(String[] strs) {
		if(strs.length==1) return strs[0];
		if(strs.length==0) return "";
    	int minLenth=Integer.MAX_VALUE;
    	for(String s: strs)
    		if(s.length()<minLenth)
    			minLenth=s.length();
    	StringBuilder sb=new StringBuilder();
    	for(int i=0;i<minLenth;i++)
    	{
    		char c=strs[0].charAt(i);
    		boolean equal=true;
        	for(String s: strs)
        		if(c!=s.charAt(i))
            	{
        			equal=false;
        			break;
            	}
        	if(equal)
        		sb.append(c);
        	else
        		break;
    	}
    	return sb.toString();        
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.longestCommonPrefix(new String [] {"flower","flow","flight"}));
	}

}
