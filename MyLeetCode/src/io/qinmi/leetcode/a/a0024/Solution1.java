package io.qinmi.leetcode.a.a0024;

/**
 * @author NEO
 	解题思路：
	有个哑节点处理就简单了，实际项目应该考虑泄露问题
 */
public class Solution1 {
	public class ListNode {
		int val;
		ListNode next;
		ListNode(int x) {
			val = x;
		}
	}
    public ListNode swapPairs(ListNode head) {
    	ListNode dummy=new ListNode(0);
    	dummy.next=head;
    	ListNode last=dummy;
    	while(last!=null)
		{
			if (last.next != null && last.next.next != null) {
				ListNode n1 = last.next;
				ListNode n2 = n1.next;
				ListNode next = n2.next;
				last.next = n2;
				n2.next = n1;
				n1.next = next;
				last = n2;
			}
			else
				last=null;
		}
    	return dummy.next;
    }
}
