/**
 * https://leetcode-cn.com/problems/reverse-integer/
 * reverse-integer 整数反转 
给定一个范围为 32 位 int 的整数，将其颠倒。
例 1:
输入: 123
输出:  321

例 2:
输入: -123
输出: -321

例 3:
输入: 120
输出: 21
 */

package io.qinmi.leetcode.a.a0007;