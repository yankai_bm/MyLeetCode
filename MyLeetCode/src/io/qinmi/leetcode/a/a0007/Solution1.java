package io.qinmi.leetcode.a.a0007;

import java.util.Stack;

/**
 * @author NEO
 	解题思路：
 	用一个栈来处理颠倒再适合不过了，
 	注意值域，注意判空
 */
public class Solution1 {
    public int reverse(int num) {
    	int p=num>0?1:-1;
    	num=Math.abs(num);
    	Stack<Integer> s=new Stack();
    	while(num>0)
    	{
    		s.push(num%10);
    		num=num/10;
    	}
    	long res=0,j=0;
    	while(!s.isEmpty())
    	{
    		res=(long) (s.pop()*Math.pow(10, j++)+res);
    	}
    	res=res*p;
    	return res>Integer.MAX_VALUE||res<Integer.MIN_VALUE?0:(int)res;
    }

	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println((long)Math.pow(2,31));
//		System.out.println(s.reverse(321));
//		System.out.println(s.reverse(-123));
//		System.out.println(s.reverse(120));
		System.out.println(s.reverse(1534236469));
	}

}
