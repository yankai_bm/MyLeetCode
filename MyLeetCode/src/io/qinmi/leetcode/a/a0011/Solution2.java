package io.qinmi.leetcode.a.a0011;

/**
 * @author NEO
 	解题思路：
	暴力搜索
 */
public class Solution2 {
	public int maxArea(int[] height) {
		int max = 0;
		for (int i = 0; i < height.length - 1; i++)
			for (int j = i + 1; j< height.length; j++) {
				int area = (j - i) * (height[i] > height[j] ? height[j] : height[i]);
				max = max > area ? max : area;
			}
		return max;

	} 
	public static void main(String[] args) {
		Solution2 s=new Solution2();
		int[] height=new int[] {1,8,2,2,5,2,5,2,5,9,8,1};
		System.out.println(s.maxArea(height));
	}

}
