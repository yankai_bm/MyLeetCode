package io.qinmi.leetcode.a.a0011;

/**
 * @author NEO
 	解题思路：
	暴力搜索
 */
public class Solution1 {
	public int maxArea(int[] height) {
	    int _m = 0, r = height.length-1,l=0;
	    while (r > l) {
	        _m = max(_m, min(height[l], height[r])*(r - l));
	        if (height[r] > height[l]) 
	        	++l;
	        else
	        	--r;
	    }
	    return _m;
	} 
	private int min(int i, int j) {
		return i>j?j:i;
	}
	private int max(int i, int j) {
		return i<j?j:i;
	}
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		int[] height=new int[] {8,1,2,2,5,2,5,2,5,9,1,8};
		System.out.println(s.maxArea(height));
	}

}
