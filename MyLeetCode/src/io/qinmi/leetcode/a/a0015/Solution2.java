package io.qinmi.leetcode.a.a0015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
	3个数之和是0，可以理解为确定一个数，寻找另外两个数的和是这个确定数的相反数
 */
public class Solution2 {

	private Set<String> set=new HashSet<String>();
	public boolean exist(int a,int b)
	{
		String s=new StringBuilder().append(a).append(b).toString();
		if(set.contains(s.toString()))
			return true;
		else
		{
			set.add(s);
			return false;
		}
	}
	public List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		Arrays.sort(nums);
		
		for (int i = 0; i < nums.length - 2; i++)
		{
			int sum=-nums[i];
			int j=i+1,k=nums.length-1;
			while(j<k)
			{
				if(nums[j]+nums[k]==sum&&!exist(nums[i],nums[j]))
				{
					List<Integer> list=new ArrayList<Integer>();
					list.add(nums[i]);
					list.add(nums[j]);
					list.add(nums[k]);
					result.add(list);
				}
				else if(sum<nums[j]+nums[k])k--;					
				else j++;
			}
				
		}
		return result;
	}
	public static void main(String[] args) {
		Solution2 s=new Solution2();
		System.out.println(s.threeSum(new int [] {-1, 0, 1, 2, -1, -4}));
	}
}
