package io.qinmi.leetcode.a.a0015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
	暴力搜索,三重循环,会超时
 */
public class Solution1 {
	private Set<String> set=new HashSet<String>();
	public boolean exist(int[] arr)
	{
		String s=new StringBuilder().append(arr[0]).append(arr[1]).toString();
		if(set.contains(s.toString()))
			return true;
		else
		{
			set.add(s);
			return false;
		}
	}
	public List<Integer> sortAndFill(int a,int b, int c)
	{	
		int[] arr=new int[] {a,b,c};
		Arrays.sort(arr);
		if(!exist(arr))
		{
			List<Integer> list = new ArrayList<Integer>();
			list.add(arr[0]);
			list.add(arr[1]);
			list.add(arr[2]);
			return list;
		}
		else
			 return null;
	}
	public List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		for (int i = 0; i < nums.length - 2; i++)
			for (int j = i + 1; j < nums.length - 1; j++)
				for (int k = j + 1; k < nums.length; k++)
					if (0 == nums[i] + nums[j] + nums[k]) {
						List<Integer> list=sortAndFill(nums[i],nums[j] ,nums[k]);
						if(list!=null)
							result.add(list);
					}
		return result;
	}
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.threeSum(new int [] {-1, 0, 1, 2, -1, -4}));
	}
}
