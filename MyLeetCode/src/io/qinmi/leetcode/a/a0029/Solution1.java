package io.qinmi.leetcode.a.a0029;

/**
 * @author NEO
 	解题思路：
	减法计数
 */
public class Solution1 {
    public int divide(int dividend, int divisor) {
        if(dividend==0) return 0;
        boolean nagtive=dividend>0^divisor>0?true:false;
        long a=Math.abs((long)dividend);
        long b=Math.abs((long)divisor);
        long result=0;
        while(a>=b)
        {
        	a=a-b;
        	result++;
        }
        if(nagtive)
        	result=-result;
        
        if(result>Integer.MAX_VALUE||result<Integer.MIN_VALUE)
        	result=Integer.MAX_VALUE;
        return (int) result;
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();		
		System.out.println(s.divide(-2147483648, -1));
	}
}
