package io.qinmi.leetcode.a.a0019;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NEO
 	解题思路：
	两个间隔N的指针，同时走，当先走的指针到尾部时，后指针刚好到要删除的指针处
 */
public class Solution2 {
	public class ListNode {
		int val;
		ListNode next;
		public ListNode getNext() {
			return next;
		}
		public void setNext(ListNode next) {
			this.next = next;
		}
		ListNode(int x) {
			val = x;
		}
	}

	public ListNode removeNthFromEnd(ListNode head, int n) {
		ListNode i1=head;
		for(int i=0;i<n;i++)
			i1=i1.next;
		ListNode i2=head;
		if(i1==null)
		{//删除头，还要返回头
			head=head.next;
			i2.next=null;
			return head;
		}
		while(i1.next!=null)
		{
			i1=i1.next;
			i2=i2.next;
		}
		//借用i1变量
		i1=i2.next;
		i2.next=i1.next;
		i1.next=null;
		return head;
	}

	public static void main(String[] args) {
		Solution2 s = new Solution2();
//		ListNode head=s.new ListNode(1);
//		s.removeNthFromEnd(head, 1);

		ListNode head=s.new ListNode(1);
		ListNode l1=s.new ListNode(2);
		head.setNext(l1);
//		ListNode l2=s.new ListNode(3);
//		l1.setNext(l2);
//		ListNode l3=s.new ListNode(4);
//		l2.setNext(l3);
//		ListNode l4=s.new ListNode(5);
//		l3.setNext(l4);
		s.removeNthFromEnd(head, 1);
	}
}
