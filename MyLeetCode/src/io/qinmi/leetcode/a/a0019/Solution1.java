package io.qinmi.leetcode.a.a0019;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NEO
 	解题思路：
	Map辅助一下，可以随机访问，只扫描一遍，另外注意如果删掉的是头，还需要返回头
 */
public class Solution1 {
	public class ListNode {
		int val;
		ListNode next;
		public ListNode getNext() {
			return next;
		}
		public void setNext(ListNode next) {
			this.next = next;
		}
		ListNode(int x) {
			val = x;
		}
	}

	private Map<Integer,ListNode> map=new HashMap<Integer,ListNode>(); 
	public ListNode removeNthFromEnd(ListNode head, int n) {
		ListNode it=head;
		int i= 0;
		while(it!=null)
		{
			map.put(i++, it);			
			it=it.next;
		}
		if(n==i)
		{//删除头，还要返回头
			it=head;
			head=head.next;
			it.next=null;
		}
		else
		{
			it=map.get(i-n);
			map.get(i-n-1).next=it.next;
			it.next=null;
		}
		return head;
	}

	public static void main(String[] args) {
		Solution1 s = new Solution1();
//		ListNode head=s.new ListNode(1);
//		s.removeNthFromEnd(head, 1);

		ListNode head=s.new ListNode(1);
		ListNode l1=s.new ListNode(2);
		head.setNext(l1);
//		ListNode l2=s.new ListNode(3);
//		l1.setNext(l2);
//		ListNode l3=s.new ListNode(4);
//		l2.setNext(l3);
//		ListNode l4=s.new ListNode(5);
//		l3.setNext(l4);
		s.removeNthFromEnd(head, 1);
	}
}
