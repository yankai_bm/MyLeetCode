package io.qinmi.leetcode.a.a0005;

/**
 * @author NEO
 	解题思路：
 	Manacher算法
 */
public class Solution2 {
    public String init(String s)
    {
    	StringBuilder sb=new StringBuilder();
    	sb.append("$#");
    	for(char c:s.toCharArray())
    	{
    		sb.append(c);
    		sb.append('#');
    	}
		sb.append('@');
    	return sb.toString();
    }
    public String longestPalindrome(String s) {
    	int index=0,id=0;
        String s_new = init(s);  // 取得新字符串长度并完成向 s_new 的转换
        int[] p=new int[s_new.length()];
        int max_len = -1;  // 最长回文长度
        int mx = 0;
        for (int i = 1; i < s_new.length()-1; i++)
        {
            if (i < mx)
                p[i] =Math.min(p[2 * id - i], mx - i);  // 需搞清楚上面那张图含义, mx 和 2*id-i 的含义
            else
                p[i] = 1;

            while (s_new.charAt(i - p[i])== s_new.charAt(i + p[i]))  // 不需边界判断，因为左有'$',右有'\0'
                p[i]++;

            // 我们每走一步 i，都要和 mx 比较，我们希望 mx 尽可能的远，这样才能更有机会执行 if (i < mx)这句代码，从而提高效率
            if (mx < i + p[i])
            {
                id = i;
                mx = i + p[i];
            }
            if( p[i] - 1>max_len)
            {
            	max_len=p[i] - 1;
            	index=i;
            }
        }
        return s_new.substring(index-max_len, index+max_len).replaceAll("#", "");
    }
	public static void main(String[] args) {
		System.out.println(new Solution2().longestPalindrome("a"));
	}

}
