package io.qinmi.leetcode.a.a0005;

/**
 * @author NEO
 	解题思路：
 	假设字符串S是回文 cSc形式的字符串也是回文串，即中心扩展，可以递归实现包括两类中心一种是具体某个字符，一种是两个相同的字符之间
 */
public class Solution1 {
    public String longestPalindrome(String s) {
    	if("".equals(s))		return s;
    	int maxLenth=0,temp=0;
    	int index=0;
    	boolean odd=true;
    	for(int i=0;i<s.length();i++)
    	{
			temp=getPalindrome(s,i,i);
			if(temp>maxLenth)
			{
				maxLenth=temp;
				index=i;
				odd=true;
			}
    		if(i<s.length()-1&&(s.charAt(i+1)==s.charAt(i)))
    		{
    			temp=getPalindrome(s,i,i+1);
    			if(temp>maxLenth)
    			{
    				maxLenth=temp;
    				index=i;
    				odd=false;
    			}
    		}
    	}
    	if(odd)
    		return s.substring(index-maxLenth/2, index+maxLenth/2+1);
    	else
    		return s.substring(index-maxLenth/2+1, index+maxLenth/2+1);
    }

    public int getPalindrome(String s,int start,int end)
    {
    	if(start==0||end==s.length()-1)
	    	return end-start+1;
    	if(s.charAt(start-1)==s.charAt(end+1))
    		return getPalindrome(s,start-1,end+1);
    	else 
    		return end-start+1;
    }
     

	public static void main(String[] args) {
		System.out.println(new Solution1().longestPalindrome("a"));
	}

}
