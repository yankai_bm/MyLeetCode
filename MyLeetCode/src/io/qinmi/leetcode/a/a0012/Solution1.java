package io.qinmi.leetcode.a.a0012;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NEO
 	解题思路：
	暴力搜索
 */
public class Solution1 {
	private Map<Integer,String> map=new HashMap<Integer, String>(); 
    public void init() {
        map.put(1, "I");
        map.put(4, "IV");
        map.put(5, "V");
        map.put(9, "IX");
        map.put(10, "X");
        map.put(40, "XL");
        map.put(50, "L");
        map.put(90, "XC");
        map.put(100, "C");
        map.put(400, "CD");
        map.put(500, "D");
        map.put(900, "CM");
        map.put(1000, "M");
    }
    public void deal(int h,int ten,StringBuilder sb) {
    	if(h>0)
        {
        	String temp=map.get(h*ten);
        	if(temp!=null)
        		sb.append(temp);
        	else
        		if(h>5)
        		{
            		sb.append(map.get(5*ten));
                	for(int i=5;i<h;i++)
                		sb.append(map.get(ten));
        		}
        		else
        		{
                	for(int i=0;i<h;i++)
                		sb.append(map.get(ten));        			
        		}
        }
    }

    public String intToRoman(int num) {
    	init();
//    	千位 最大为3
        int th=num/1000;
//    	百位
        int h=(num-th*1000)/100;
//    	十位
        int t=(num-th*1000-h*100)/10;
//    	个位
        int o=num-th*1000-h*100-t*10;
        StringBuilder sb=new StringBuilder();
        deal(th,1000,sb);
        deal(h,100,sb);
        deal(t,10,sb);
        deal(o,1,sb);
        	
        return sb.toString();
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.intToRoman(1994));
	}

}
