package io.qinmi.leetcode.a.a0010;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author NEO
 	解题思路：
 	先对P做分解，然后逐个字符对s做匹配
 	用递归来搞定,针对通配符*处理相对复杂一些，
 */
public class Solution1 {

    public String[] initPattern(String p) {
    	List<String> list =new LinkedList<String>();
    	for(int i=0;i<p.length();i++)
    	{
    		if(i<p.length()-1&&p.charAt(i+1)=='*')
    			list.add(String.valueOf(new char[]{p.charAt(i),p.charAt(++i)}));
    		else
    			list.add(String.valueOf(p.charAt(i)));
    	}    	
    	return list.toArray(new String[list.size()]);  			
    }
    /**
     * @param s 待匹配原始字符串
     * @param s_index 匹配到字符串的哪一个字符
     * @param patterns[] 拆解分离后的Pattern串列表
     * @param p_index 拆解分离后的Pattern串列表的指针
     * @return
     */
	public boolean match(String s, int s_index, String[] patterns, int p_index) {
		if (s_index == s.length() && p_index == patterns.length)
			return true;		//字符串和pattern都匹配结束，则成功
		if (s_index > s.length() || p_index > patterns.length)
			return false;//字符串和pattern任意一个超长都失败
		if (s_index < s.length() && p_index==patterns.length)
			return false;//字符串有值而pattern为空则匹配失败
		String pattern = patterns[p_index];
		if (s_index == s.length() && p_index<patterns.length)
		//字符串已经是空串，pattern还有数据，则只有pattern后续都为*时才可以匹配
			if(pattern.endsWith("*"))
				return match(s,s_index,patterns,p_index+1);
			else
				return false;
		//所有空串和空pattern情况都处理完毕，后续开始主体匹配逻辑		
		//单字符.通配符，直接通过
		if(".".equals(pattern))
			return match(s,s_index+1,patterns,p_index+1);
		//单字符字母，直接逐个字符匹配即可
		if (!pattern.endsWith("*"))
			if(s.charAt(s_index)==pattern.charAt(0))
				return match(s,s_index+1,patterns,p_index+1);
			else
				return false;
		//*通配符 分两种情况，任意一种通过即可
		boolean matched = false;
		if (pattern.endsWith("*")) {
			//*匹配通过，包括字符匹配和.*匹配
			if (s.charAt(s_index) == pattern.charAt(0)||'.'== pattern.charAt(0)) {
				// pattern和字符都匹配通过
				matched = matched || match(s, s_index + 1, patterns, p_index + 1);
				// 字符匹配通过，pattern留下继续匹配
				matched = matched || match(s, s_index + 1, patterns, p_index);
			}
			// 字符留下继续匹配，pattern匹配通过
			matched = matched || match(s, s_index, patterns, p_index + 1);
		}
		return matched;
	}
    public boolean isMatch(String s, String p) {
    	String[] patterns=initPattern(p);
        return match(s,0,patterns,0);
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		String[] list=s.initPattern(".*");
		for(String sub:list)
			System.out.println(sub);
		System.out.println(s.isMatch("aa", "a"));
		System.out.println(s.isMatch("aa", "a*"));
		System.out.println(s.isMatch("ab", ".*"));
		System.out.println(s.isMatch("aab", "c*a*b"));
		System.out.println(s.isMatch("mississippi", "mis*is*p*."));

	}

}
