package io.qinmi.leetcode.a.a0016;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author NEO
 	解题思路：
	暴力搜索,三重循环,以为会超时,然而并没有
 */
public class Solution2 {
	public int threeSumClosest(int[] nums, int target) {
		int sum = 0;
		Arrays.sort(nums);		
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < nums.length - 2; i++)
		{
			int negative = target - nums[i];
			int j = i + 1, k = nums.length - 1;
			while (j < k) {
				if (nums[j] + nums[k] == negative)
					return target;
				else {
					int gap = Math.abs(nums[j] + nums[k] - negative);
					if (min > gap) {
						sum = nums[i] + nums[j] + nums[k];
						min = gap;
					}
					if (negative < nums[j] + nums[k]) {
						k--;
					} else {
						j++;
					}
				}

			}
		}
		return sum;
	}
	public static void main(String[] args) {
		Solution2 s=new Solution2();
		System.out.println(s.threeSumClosest(new int [] {0,2,1,-3},1));
	}
}
