package io.qinmi.leetcode.a.a0016;


/**
 * @author NEO
 	解题思路：
	暴力搜索,三重循环,以为会超时,然而并没有
 */
public class Solution1 {
	public int threeSumClosest(int[] nums, int target) {
		int sum = 0;
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < nums.length - 2; i++)
			for (int j = i + 1; j < nums.length - 1; j++)
				for (int k = j + 1; k < nums.length; k++) {
					int gap = Math.abs(nums[i] + nums[j] + nums[k] - target);
					if (min > gap) {
						sum = nums[i] + nums[j] + nums[k];
						min = gap;
						if (min == 0)
							return target;
					}
				}
		return sum;
	}
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.threeSumClosest(new int [] {-1,2,1,-4},1));
	}
}
