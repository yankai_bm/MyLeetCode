package io.qinmi.leetcode.a.a0008;

/**
 * @author NEO
 	解题思路：
 	注意值域，注意判空
 */
public class Solution1 {
    public int myAtoi(String str) {
        str=str.trim();
        if("".equals(str)) return 0;
        int p=1;
        if(str.charAt(0)=='+')
        {
        	str=str.substring(1);
        }
        else if(str.charAt(0)=='-')
        {
        	str=str.substring(1);
            p=-1;
        }
        if("".equals(str)) return 0;
        if(str.charAt(0)>'9'||str.charAt(0)<'0')
        	return 0;
        long num=0;
        int i=0;
        for(;i<str.length()&&str.charAt(i)>='0'&&str.charAt(i)<='9';i++)
        {
        	num=num*10+(str.charAt(i)-'0')*p;
        	if(num>Integer.MAX_VALUE)
        		return Integer.MAX_VALUE;
        	if(num<Integer.MIN_VALUE)
        		return Integer.MIN_VALUE;
        }
    	return (int)num;
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.myAtoi("+"));
	}

}
