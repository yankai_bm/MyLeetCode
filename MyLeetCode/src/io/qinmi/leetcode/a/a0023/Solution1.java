package io.qinmi.leetcode.a.a0023;

/**
 * @author NEO
 	解题思路：
	两两归并，直至全部归并为一个链表，比K路直接比较简单而且更快
 */
public class Solution1 {
	public class ListNode {
		int val;
		ListNode next;
		ListNode(int x) {
			val = x;
		}
	}
	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		ListNode dummy=new ListNode(0);
		ListNode current=dummy;
		while(l1!=null&&l2!=null)
		{

			if (l1.val > l2.val) {
				current.next = l2;
				current = l2;
				l2 = l2.next;
			} else {
				current.next = l1;
				current = l1;
				l1 = l1.next;
			}
		}
		if (l1 == null) {
			current.next = l2;
		}
		if (l2 == null) {
			current.next = l1;
		}
		return dummy.next;
	}

	public ListNode mergeKLists(ListNode[] lists) {
		if(lists.length<=0) return null;
		ListNode[] before = lists;
		ListNode[] after = null;
		while (before.length > 1) {
			after = new ListNode[(before.length + 1) / 2];
			for (int i = 0; i < after.length; i++)
				if ((i+1) * 2 <= before.length)
					after[i] = mergeTwoLists(before[2 * i], before[2 * i + 1]);
				else
					after[i] = before[2 * i];
			before = after;
		}
		return before[0];
	}
}
