package io.qinmi.leetcode.a.a0009;

/**
 * @author NEO
 	解题思路：
 	借用一下第五题的方法~
 */
public class Solution1 {
	public boolean isPalindrome(int x) {
        if(x<0)
        	return false;
        io.qinmi.leetcode.a.a0005.Solution1 s15=new io.qinmi.leetcode.a.a0005.Solution1();
        String p=s15.longestPalindrome(String.valueOf(x));
        if(String.valueOf(x).equals(p))
        	return true;
        else
        	return false;
    }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.isPalindrome(12721));
	}

}
