package io.qinmi.leetcode.a.a0009;

/**
 * @author NEO
 	解题思路：
 	反转数字需要考虑溢出问题
 */
public class Solution3 {
	public boolean isPalindrome(int x) {
		if (x < 0)
			return false;
		int temp = x, l = 0;
		int[] n = new int[10];// 最大的int就是10位
		while (temp > 0) {
			n[l] = temp % 10;
			temp = temp / 10;
			l++;
		}
		for (int i = 0; i < l / 2; i++)
			if (n[i] != n[l - 1 - i])
				return false;
		return true;
	}
	public static void main(String[] args) {
		Solution3 s=new Solution3();
		System.out.println(s.isPalindrome(127121));
	}

}
