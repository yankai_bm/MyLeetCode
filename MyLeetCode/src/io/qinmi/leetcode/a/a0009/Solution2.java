package io.qinmi.leetcode.a.a0009;

/**
 * @author NEO
 	解题思路：
 	借用一下第七题的方法~
 */
public class Solution2 {
	public boolean isPalindrome(int x) {
        if(x<0)
        	return false;
        io.qinmi.leetcode.a.a0007.Solution1 s17=new io.qinmi.leetcode.a.a0007.Solution1();
        int y=s17.reverse(x);
        if(x==y)
        	return true;
        else
        	return false;
    }
	public static void main(String[] args) {
		Solution2 s=new Solution2();
		System.out.println(s.isPalindrome(12721));
	}

}
