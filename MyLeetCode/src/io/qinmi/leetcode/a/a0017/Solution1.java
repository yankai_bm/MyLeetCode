package io.qinmi.leetcode.a.a0017;

import java.util.ArrayList;
import java.util.List;

/**
 * @author NEO
 	解题思路：
	递归遍历所有空间即可
 */
public class Solution1 {
	public String[] map=new String['9'+1];
	public void initMap()
	{
		map['2'] = "abc";
		map['3'] = "def";
		map['4'] = "ghi";
		map['5'] = "jkl";
		map['6'] = "mno";
		map['7'] = "pqrs";
		map['8'] = "tuv";
		map['9'] = "wxyz";
	}
	public void combin(String digits,int index,char[] chars,List<String> list)
	{
		
		String s = map[digits.charAt(index)];
		for (int i = 0; i < s.length(); i++) {
			chars[index] = s.charAt(i);
			if (index == chars.length-1) {
				list.add(String.valueOf(chars));
			}
			else
			{
		    	combin( digits, index+1, chars, list);
			}
		}
	}
    public List<String> letterCombinations(String digits) {
    	
    	char[] chars=new char[digits.length()];
    	initMap();
    	List<String> list=new ArrayList<String>();
    	if(0==chars.length) return list;
    	combin( digits, 0, chars, list);
		return list; 
       }
	public static void main(String[] args) {
		Solution1 s=new Solution1();
		System.out.println(s.letterCombinations("23"));
	}
}
