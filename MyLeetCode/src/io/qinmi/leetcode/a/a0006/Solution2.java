package io.qinmi.leetcode.a.a0006;

/**
 * @author NEO
 	解题思路：
	和上一个解基本一样，改用StringBuilder对象
 */
public class Solution2 {
    public String convert(String s, int numRows) {
    	if(numRows==1) return s;
    	StringBuilder[] lists=new StringBuilder[numRows];
        for(int i=0;i<lists.length;i++)
        	lists[i]=new StringBuilder();
        int N=numRows-1;
        for(int i=0;i<s.length();i++)
        {
        	if(i%(N*2)==0)
        		lists[0].append(s.charAt(i));
        	else if(i%N==0)
        		lists[N].append(s.charAt(i));
        	else if(i%(N*2)>N)
        		lists[N-i%N].append(s.charAt(i));
        	else
        		lists[i%N].append(s.charAt(i));        		
        }
        StringBuilder sb=new StringBuilder();
        for(StringBuilder l:lists)
        		sb.append(l);        	
        return sb.toString();
    }

	public static void main(String[] args) {
		System.out.println(new Solution2().convert("LEETCODEISHIRING",4));
	}

}
