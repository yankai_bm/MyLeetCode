package io.qinmi.leetcode.a.a0006;

import java.util.LinkedList;
import java.util.List;

/**
 * @author NEO
 	解题思路：
 	每一次向下经过N-1个字符，再向上又是N-1个字符，所以每次循环是（N-1）*2个字符
 	从0开始最上面的字符都是(N-1)*2*k 最下面字符是(N-1)*(2*k+1)
 */
public class Solution1 {
    public String convert(String s, int numRows) {
    	if(numRows==1) return s;
        List<Character>[] lists=new List[numRows];
        for(int i=0;i<lists.length;i++)
        	lists[i]=new LinkedList<Character>();
        int N=numRows-1;
        for(int i=0;i<s.length();i++)
        {
        	if(i%(N*2)==0)
        		lists[0].add(s.charAt(i));
        	else if(i%N==0)
        		lists[N].add(s.charAt(i));
        	else if(i%(N*2)>N)
        		lists[N-i%N].add(s.charAt(i));
        	else
        		lists[i%N].add(s.charAt(i));        		
        }
        StringBuilder sb=new StringBuilder();
        for(List<Character> l:lists)
        	for(Character c:l)
        		sb.append(c);        	
        return sb.toString();
    }

	public static void main(String[] args) {
		System.out.println(new Solution1().convert("LEETCODEISHIRING",4));
	}

}
