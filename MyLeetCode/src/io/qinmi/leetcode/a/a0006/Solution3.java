package io.qinmi.leetcode.a.a0006;

/**
 * @author NEO
 	解题思路：
	把各行的起始位置都算出来，然后按照Z字形逐个填充
 */
public class Solution3 {
    public String convert(String s, int numRows) {
    	if(numRows==1) return s;
        int N=numRows-1,l=s.length();        
    	int[][] sections=new int[numRows][2];
    	sections[0]= new int[]{0,(l-1)/(N*2)};
    	sections[N]= new int[]{l-((l-1)/N+1)/2,l-1};
    	int start=(l-1)/(N*2)+1;
        for(int i=1;i<sections.length-1;i++)
        {
        	sections[i]=new int[2];
        	sections[i][0]=start;
        	sections[i][1]=start+l/(N*2)*2-1;
        	if(l%(N*2)>i)sections[i][1]++;
        	if(l%(N*2)>(2*N-i))sections[i][1]++;
        	start=sections[i][1]+1;
        }
    	int[] indexes=new int[numRows];
        for(int i=0;i<indexes.length;i++)
        	indexes[i]=sections[i][0];
        int direction=1;//1为向下 -1为向上
        int j=0;
        char[] new_s= new char[l];
        for(int i=0;i<s.length();i++)
        {
        	new_s[indexes[j]++]=s.charAt(i);
        	j+=direction;
        	if(j==0)direction=1;
        	if(j==N)direction=-1;
        }
        return new String(new_s);
    }

	public static void main(String[] args) {
		System.out.println(new Solution3().convert("PAYPALISHIRING",3));
	}

}
