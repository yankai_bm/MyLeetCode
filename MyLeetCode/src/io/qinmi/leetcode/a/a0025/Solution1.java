package io.qinmi.leetcode.a.a0025;

/**
 * @author NEO
 	解题思路：
	加个哑节点处理就简单了，用数组来反转
 */
public class Solution1 {
	public class ListNode {
		int val;
		ListNode next;
		ListNode(int x) {
			val = x;
		}
	}
    public ListNode reverseKGroup(ListNode head, int k) {
    	if(k<=1) return head;
    	ListNode dummy=new ListNode(0);
    	dummy.next=head;
    	ListNode last=dummy;
		ListNode[] nodes= new ListNode[k];
    	while(last!=null)
		{
    		ListNode node=last;
    		boolean enough=true;
    		for(int i=0;i<k;i++)
    		{
    			nodes[i]=node.next;   			
    			if(nodes[i]==null)
    			{
    				enough=false;
    				break;
    			}
    			else
    			{
    				node=node.next;
    			}
    		}
    		if(enough)
    		{
    			last.next=nodes[k-1];
    			node=nodes[k-1].next;
    			for(int i=k-1;i>0;i--)
    			{
    				nodes[i].next=nodes[i-1];
    			}
    			nodes[0].next=node;
    			last=nodes[0];
    		}
			else
				last=null;
		}
    	return dummy.next;
    }
}
