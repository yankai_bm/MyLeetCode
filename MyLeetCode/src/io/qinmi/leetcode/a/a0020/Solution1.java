package io.qinmi.leetcode.a.a0020;

import java.util.Stack;

/**
 * @author NEO
 	解题思路：
	用栈实现吧
 */
public class Solution1 {
    public boolean isValid(String s) {
    	Stack<Character> stack=new Stack<Character>();
    	for(int i=0;i<s.length();i++)
    	{
    		char temp=s.charAt(i);
    		if(temp=='['||temp=='('||temp=='{') 
    			stack.push(temp);
    		else
    		{
    			if(stack.isEmpty()) return false;
    			char c=stack.pop();
    			if(c=='['&&temp==']')continue;
    			if(c=='('&&temp==')')continue;
    			if(c=='{'&&temp=='}')continue;
    			return false;
    		}
    	}
    	if(!stack.isEmpty()) return false;
        return true;
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.isValid("(([]){})"));
	}
}
