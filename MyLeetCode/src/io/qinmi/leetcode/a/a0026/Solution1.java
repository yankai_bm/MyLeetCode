package io.qinmi.leetcode.a.a0026;

/**
 * @author NEO
 	解题思路：
	从左向右判断，两个指针一个是判断指针，另一个是填充指针
 */
public class Solution1 {
    public int removeDuplicates(int[] nums) {
    	if(nums.length==0) return 0;
    	int k=0;
    	int last=nums[0];
        for(int i=0;i<nums.length;i++)
        {
        	if(nums[i]!=last)
        	{
        		nums[++k]=nums[i];
        		last=nums[i];
        	}
        	
        }
        return k+1;
    }
}
