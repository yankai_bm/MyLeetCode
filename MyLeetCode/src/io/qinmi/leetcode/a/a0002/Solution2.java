package io.qinmi.leetcode.a.a0002;
/**
 * @author NEO
 	解题思路：
 	同S1，考虑到三个循环有点傻，精简代码
 */
public class Solution2 {

	public class ListNode {
		int val;
		ListNode next;
		public ListNode(int x) {
			val = x;
		}
	}

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
    	//i1\i2\i3是三个遍历变量
    	ListNode i1= l1, i2= l2,root=new ListNode(-999),i3=root;
    	int overTen=0;
    	//是否进位 0不进位 1 进位
    	while(i1!=null||i2!=null)
    	{        
    		int x = (i1 != null) ? i1.val : 0;
    		int y = (i2 != null) ? i2.val : 0;
    		int sum = overTen + x + y;
    		overTen = sum / 10;
    		i3.next = new ListNode(sum % 10);
    		i3 = i3.next;
            if (i1 != null) i1 = i1.next;
            if (i2 != null) i2 = i2.next;
    	}
        if (overTen > 0) {
        	//如果最后有进位则增加
            i3.next = new ListNode(overTen);
        }
        i3=root.next;
        root.next=null;
		return i3;
    }

	public static void main(String[] args) {
		Solution2 s1=new Solution2();
		ListNode l1 = s1.new ListNode(1);
		ListNode l2 = s1.new ListNode(9);
		l2.next=s1.new ListNode(9);
		ListNode l3=s1.addTwoNumbers(l1, l2);
		while(l3!=null)
		{
			System.out.print(l3.val+" ");
			l3=l3.next;
		}

	}

}
