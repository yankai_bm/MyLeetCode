package io.qinmi.leetcode.a.a0002;
/**
 * @author NEO
 	解题思路：
 	链表逐个位合并即可，大于十则进位
 */
public class Solution1 {

	public class ListNode {
		int val;
		ListNode next;
		public ListNode(int x) {
			val = x;
		}
	}

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
    	ListNode i1= l1, i2= l2,i3=new ListNode(-999),l3=i3;
    	int overTen=0;
    	//是否进位 0不进位 1 进位
    	while(i1!=null&&i2!=null)
    	{
    		i3.next=new ListNode(i1.val+i2.val+overTen);
    		i3=i3.next;
    		if(i3.val>=10)
    		{//大于十则取个位，并且进位
    			i3.val=i3.val-10;
    			overTen=1;
    		}
    		else
    		{
    			overTen=0;
    		}
    		i1=i1.next;
    		i2=i2.next;
    	}
    	while(i1!=null)
    	{
    		i3.next=new ListNode(i1.val+overTen);
    		i3=i3.next;
    		if(i3.val>=10)
    		{
    			i3.val=i3.val-10;
    			overTen=1;
    		}
    		else
    		{
    			overTen=0;
    		}
    		i1=i1.next;
    	}
    	while(i2!=null)
    	{
    		i3.next=new ListNode(i2.val+overTen);
    		i3=i3.next;
    		if(i3.val>=10)
    		{
    			i3.val=i3.val-10;
    			overTen=1;
    		}
    		else
    		{
    			overTen=0;
    		}
    		i2=i2.next;
    	}
    	if(overTen==1)
    		i3.next=new ListNode(1);
    	l3=l3.next;    	
		return l3;
    }

	public static void main(String[] args) {
		Solution1 s1=new Solution1();
		ListNode l1 = s1.new ListNode(1);
		ListNode l2 = s1.new ListNode(9);
		l2.next=s1.new ListNode(9);
		ListNode l3=s1.addTwoNumbers(l1, l2);
		while(l3!=null)
		{
			System.out.print(l3.val+" ");
			l3=l3.next;
		}

	}

}
