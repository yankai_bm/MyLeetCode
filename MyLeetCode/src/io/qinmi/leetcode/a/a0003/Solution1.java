package io.qinmi.leetcode.a.a0003;

import java.util.HashSet;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
 	暴力搜索
 */
public class Solution1 {

    public int lengthOfLongestSubstring(String s) {
    	int max=0;
    	for(int i=0;i<s.length();i++)
    	{
    		Set<String> set=new HashSet<String>();
    		int j=i,l=0;
    		for(;j<s.length();j++)
    		{
    			if(set.contains(s.charAt(j)+""))
    				break;
    			else
    				set.add(s.charAt(j)+"");
    		}
			l=j-i;
			max=max>l?max:l;
    	}
        return max;
    }

	public static void main(String[] args) {
		
	}

}
