package io.qinmi.leetcode.a.a0003;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author NEO
 	解题思路：
 	使用Map做标记
 	如果i和j无重复字符，则i+1到j之间也没有重复字符
 */
public class Solution2 {

    public int lengthOfLongestSubstring(String s) {
    	int max=0;
    	int n=s.length();
		HashMap<Character,Integer> map=new HashMap<Character,Integer>();
        for (int j = 0, i = 0; j < n; j++) {
            if (map.containsKey(s.charAt(j))) {
                i = Math.max(map.get(s.charAt(j)), i);
            }
            max = Math.max(max, j - i + 1);
            map.put(s.charAt(j), j + 1);
        }
        return max;
    }

	public static void main(String[] args) {
		
	}

}
