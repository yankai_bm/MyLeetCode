package io.qinmi.leetcode.a.a0001;

import java.util.HashMap;

/**
 * @author NEO
 	解题思路：
 	利用Map结构，以及加法的反向运算减法，一次循环
 */
public class Solution2 {
	public static int[] twoSum(int[] nums, int target) {
		HashMap<Integer,Integer> hm=new HashMap<Integer,Integer>();
		for (int i = 0; i < nums.length; i++)
		{
			int sub=target-nums[i];
			if(hm.get(sub)!=null)
			{//如果存在，则返回结果
				return new int[] {hm.get(sub),i};
			}
			else
			{//如果不存在，则放到Map中
				hm.put(nums[i],i);
			}
		}
		//如果循环结束，依然没有解，则跑出异常
		throw new IllegalStateException("No right solution!");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] res=twoSum(new int[]{2, 7, 11, 15},9);
		System.out.println(res[0]+","+res[1]);
	}

}
