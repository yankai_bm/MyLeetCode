package io.qinmi.leetcode.a.a0001;
/**
 * @author NEO
 	解题思路：
 	双重循环，遍历所有组合。
 */
public class Solution1 {
	public static int[] twoSum(int[] nums, int target) {
		for (int i = 0; i < nums.length; i++)
			for (int j = 0; j < nums.length; j++)
				if (i != j && nums[i] + nums[j] == target) {
					int[] res = { i, j };
					return res;
				}
		throw new IllegalStateException("No right solution!");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] res=twoSum(new int[]{2, 7, 11, 15},9);
		System.out.println(res[0]+","+res[1]);
	}

}
