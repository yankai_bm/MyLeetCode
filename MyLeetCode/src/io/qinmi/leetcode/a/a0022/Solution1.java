package io.qinmi.leetcode.a.a0022;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author NEO
 	解题思路：
	暴力搜索所有情况
 */
public class Solution1 {
	public int left_count=0;
	public int right_count=0;
	/**
	 * @param n 第n个符号
	 * @param index 字符串指针
	 * @param chars 字符串
	 * @param list 结果集
	 */
	public void generate(int index,char[] chars,List<String> list)
	{
		if(index==chars.length)
		{
			String s=String.valueOf(chars);
			if(isValid(s))
				list.add(s);
			return;			
		}		
		chars[index]='(';
		generate( index+1,chars,list);
		chars[index]=')';
		generate( index+1,chars,list);	
	}
	//只要所有节点处的左括号数都大于等于右括号数且最终相等即可
    public boolean isValid(String s) {
    	
    	int left=0;
    	int right=0;
    	for(int i=0;i<s.length();i++)
    	{
    		if('('==s.charAt(i))left++;
    		if(')'==s.charAt(i))right++;
    		if(right>left)
    			return false;
    	}
    	return right==left;

    }
    public List<String> generateParenthesis(int n) {
    	List<String> list =new ArrayList<String>();
    	char[] chars=new char[n*2]; 
		generate( 0,chars,list);
		return list;
    	
    	
    }

	public static void main(String[] args) {
		Solution1 s = new Solution1();
		System.out.println(s.generateParenthesis(0));
//		System.out.println(s.isValid("((())())"));
	}
}
