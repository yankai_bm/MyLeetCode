package io.qinmi.leetcode.a.a0022;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author NEO
 	解题思路：
	保证字符串从左开始的每个节点左括号数都比右括号数多即可
 */
public class Solution2 {
	public int left_count=0;
	public int right_count=0;
	public void generate(int index,char[] chars,List<String> list)
	{
		int n = chars.length / 2;
		if (index == chars.length) {
			String s = String.valueOf(chars);
			list.add(s);
			return;
		}
		if (left_count < n) {
			chars[index] = '(';
			left_count++;
			generate(index + 1, chars, list);
			left_count--;
		}
		if (right_count<left_count) {
			right_count++;
			chars[index] = ')';
			generate(index + 1, chars, list);
			right_count--;
		}
	}

    public List<String> generateParenthesis(int n) {
    	List<String> list =new ArrayList<String>();
    	char[] chars=new char[n*2]; 
		generate( 0,chars,list);
		return list;  	
    }

	public static void main(String[] args) {
		Solution2 s = new Solution2();
		System.out.println(s.generateParenthesis(3));
//		System.out.println(s.isValid("((())())"));
	}
}
