package io.qinmi.leetcode.s.s0564;

import java.util.Arrays;

/**
 * @author NEO
 	解题思路：
 	对于一位数，固定返回减一
 	对于长度为n的字符串，有(n+1)/2位为对称的数字几位回文数，奇数位数字以中间数为对称轴，偶数位则以中间两个数之间为对称轴
 	对称数字增减即为（对奇数而言含对称轴）最近的两个回文数，分别判断距离即可。
 	另外需要考虑全是9的情况，不考虑自身，向上会导致位数变化，向下则距离较远，例如99，最近的回文数101，例如999最近的是1001，固定为+2
 	同样反过来101 1001 （对应还有100、1000）这种情况返回的也是构造99、999
 */
public class Solution1 {
	private String dealAllNine(String n)
	{
		for(char c:n.toCharArray())
			if(c!='9')
				return null;
		char[] chars=new char[n.length()+1];
		Arrays.fill(chars, '0');
		chars[0]=(chars[n.length()]='1');		
		return new String(chars);
	}
	private String dealIoooI(String n)
	{
		int l=n.length();
		if(n.charAt(0)!='1'||(n.charAt(l-1)!='1'&&n.charAt(l-1)!='0'))
			return null;
		for(int i=1;i<l-1;i++)
			if(n.charAt(i)!='0')
				return null;
		char[] chars=new char[l-1];
		Arrays.fill(chars, '9');	
		return new String(chars);
	}
    public String nearestPalindromic(String n) {
    	int l=n.length();
    	if(l==1)
    		return String.valueOf(((char)(n.charAt(0)-1)));
    	String temp=dealAllNine(n);
    	if(temp!=null)
    		return temp;
    	temp=dealIoooI(n);
    	if(temp!=null)
    		return temp;
    	int half_len=(l+1)/2;
    	int odd=l%2;
    	String half_n=n.substring(0, half_len);
    	String num1=reverse(half_n,odd);
    	if(num1.equals(n))
    	{//自身是回文数时，除非最后一位是0，否则都应当直接返回较小的值
    		if('0'==half_n.charAt(half_len-1))
    			return reverse(inc(half_n),odd);
    		else
    			return reverse(dec(half_n),odd);
    	}
    	else
		{// 不是回文数则，re是其中一个最近的值了
			long gap1 = compare(n, num1);
			// 差值大于0则另个一数是下一个回文数，否则取上一个回文数
			if (gap1 > 0) {
				String num2 = reverse(inc(half_n), odd);
				long gap2 = compare(n, num2);
				if(gap1>-gap2)
					return num2;
				else
					return num1;
			} else {
				String num2 = reverse(dec(half_n), odd);
				long gap2 = compare(n, num2);
				if(-gap1>=gap2)
					return num2;
				else
					return num1;
			}
		}
    }
    public String reverse(String n,int odd) 
    {//反转n产生一个回文数，如果是奇数则为以最后一位为对称轴反转
		StringBuilder sb=new StringBuilder();
		sb.append(n);
		for(int i=n.length()-1-odd;i>=0;i--)
			sb.append(n.charAt(i));
    	return sb.toString();
    }
    public String inc(String n) 
    {//将数字n增加1，全9的情况已经处理,不需要考虑增位的情况
    	int carry=1;
    	char[] chars=n.toCharArray();
    	for(int i=chars.length-1;i>=0;i--)
    	{
    		chars[i]= (char) (chars[i] + carry);
			if(chars[i]>'9')
			{
				chars[i]='0';
				carry=1;
			}
			else
				carry=0;
			if(carry==0)
				break;    		
    	}
		return new String(chars);
    }
    public String dec(String n) 
    {//将数字n减少1，IoooI的情况已经处理，不需要考虑减位的情况
    	int carry=1;
    	char[] chars=n.toCharArray();
    	for(int i=chars.length-1;i>=0;i--)
    	{
    		chars[i] = (char) (chars[i] - carry);
			if(chars[i]<'0')
			{
				chars[i]='9';
				carry=1;
			}
			else
				carry=0;
			if(carry==0)
				break;    		
    	}
		return new String(chars);
    }
    public long compare(String n1,String n2) 
    {//比较，并且返回差值，只需要考虑位数相同的情况,n在0到18位之间，因而long也足够了
    	long result=0;
    	int carry=0;
    	int l=n1.length();
    	for(int i=l-1;i>=0;i--)
    	{
    		int temp=n1.charAt(i)-n2.charAt(i)-carry;
    		if(temp<0)
    		{
    			temp+=10;
    			carry=1;
    		}
    		else
    			carry=0;
    		result+=Math.pow(10, l-1-i)*temp;
    	}
    	if(carry==1)
    		result=(long) (result-Math.pow(10, l));
    	return result;    			
    }
    public static void main(String[] args) {
    	Solution1 s = new Solution1();		
		System.out.println(s.nearestPalindromic("1805170081"));
//		System.out.println(345-453);
//		System.out.println(s.rob(new int[] {2,7,9,3,1}));
	}
}
