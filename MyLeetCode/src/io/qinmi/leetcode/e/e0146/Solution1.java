package io.qinmi.leetcode.e.e0146;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NEO
 	解题思路：
	暴力搜索，对于任何两个点，判断其后续的点有多少在直线上，
	x的差比上y的差为固定值，这样就是一条直线上
	由于存在重复点，所以考虑先排重
 */
public class Solution1 {
	class Point {
		int x;
		int y;
		Point() 
		{
			x = 0;
			y = 0;
		}
		Point(int a, int b) {
			x = a;
			y = b;
		}
	}
	private Map<String,Integer> counts=new HashMap<String,Integer>();
	private static String separator="_";
	private Integer put(Point key, Integer value)
	{
		if(key==null) return null;
    	String s=key.x+separator+key.y;
    	return counts.put(s, value);
	}
	private boolean containsKey(Point key)
	{
		if(key==null) return false;
    	String s=key.x+separator+key.y;
		return counts.containsKey(s);
	}
	private Integer get(Point key)
	{
		if(key==null) return null;
    	String s=key.x+separator+key.y;
		return counts.get(s);		
	}
	private Point[] getArray()
	{
		String[] key_strings= counts.keySet().toArray(new String[0]);
		Point[] result=new Point[key_strings.length];
		for(int i=0;i<key_strings.length;i++)
		{
			String[] temp=key_strings[i].split(separator);
			result[i]=new Point(Integer.parseInt(temp[0]),Integer.parseInt(temp[1]));
		}
		return result;		
	}
	public int maxPoints(Point[] points) {
		for (Point p : points)
			if (!containsKey(p))
				put(p, 1);
			else
				put(p, get(p) + 1);
		points = getArray();
		int length = points.length, max = 0,count=0;
		if (length <= 2)
		{
			for(Point p : points)
				count+=get(p);
			return count;
		}
		for (int i = 0; i < length - 2; i++)
			for (int j = i + 1; j < length - 1; j++) {
				count = get(points[j]) + get(points[i]);
				long dif_x_1 = points[j].x - points[i].x;
				long dif_y_1 = points[j].y - points[i].y;
				for (int k = j + 1; k < length; k++) {
					long dif_x_2 = points[k].x - points[i].x;
					long dif_y_2 = points[k].y - points[i].y;
					if (dif_x_1 * dif_y_2 == dif_x_2 * dif_y_1) {
						count += get(points[k]);
					}
				}
				if (max < count)
					max = count;
			}
		return max;
	}

	public static void main(String[] args) {
		Solution1 s = new Solution1();
//		System.out.println(s.maxPoints(new Point[]{s.new Point(1,1),s.new Point(3,2),s.new Point(5,3),s.new Point(4,1),s.new Point(2,3),s.new Point(1,4)}));
		System.out.println(s.maxPoints(new Point[]{s.new Point(0,0),				s.new Point(0,0)}));
//		System.out.println(s.maxPoints(new Point[]{s.new Point(0,9),				s.new Point(138,429),				s.new Point(115,359),				s.new Point(115,359),				s.new Point(-30,-102),				s.new Point(230,709),				s.new Point(-150,-686),				s.new Point(-135,-613),				s.new Point(-60,-248),				s.new Point(-161,-481),				s.new Point(207,639),				s.new Point(23,79),				s.new Point(-230,-691),				s.new Point(-115,-341),				s.new Point(92,289),				s.new Point(60,336),				s.new Point(-105,-467),				s.new Point(135,701),				s.new Point(-90,-394),				s.new Point(-184,-551),				s.new Point(150,774)}));
//		Point[] points=new Point[] {s.new Point(1,1),s.new Point(1,1),s.new Point(1,1)};
//		Map<Point,Integer> counts=new HashMap<Point,Integer>();
//		for(Point p:points)
//			if(!counts.containsKey(p))
//				counts.put(p, 1);
//			else
//				counts.put(p, counts.get(p)+1);
//		System.out.println(counts.size());
	}
}
